# React-training

# Practice one: User manager

### Overview

- Build a website for user management
- Description: user data is stored to json-server, user can use CRUD and search name
- Design:
  -User manager: https://webix.com/demos/user-manager/

### Targets

- Apply what you have read about the main concepts of ReactJS
- And know how to use Storybook
- Use Hooks

### Requirements

- Create app user manager
- CRUD
- Search, filter user
- Apply storybook

### Information

- Timeline
  - Estimate day: 10 days
  - Actual day: 14 days
- Techniques:
  - HTML/SASS(SCSS)
  - Typescript
  - Storybook
  - Vite
  - Gitlab
  - Json-server
  - Axios
  - ESLint Extension
  - Prettier Extension
  - ESLint with TypeScript

- Editor: Visual Studio Code.

### Development Environment
- Node v16.16
- pnpm v7.19.0
- ReactJS v18.2.0
- Storybook ReactJS v6.5.16
- Vite v4.1.0
- Eslint v8.8.0
- Prettier v2.7.1
- TypeScript v4.9.3
- axios v0.27.2

### Main App Feature

- User:
  - Create new user
  - View user info
  - Edit user
  - Update user
  - Delete user
  - search/filter user

### Run server

> pnpm run server ( yarn server )

### Run project

> pnpm run start ( yarn start )

follow at:

<a href="http://127.0.0.1:5173/">http://127.0.0.1:5173/</a>

### Run storybook

> pnpm run storybook

follow at:

<a href="http://localhost:6006/">http://localhost:6006/</a>

