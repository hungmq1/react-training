import { ComponentMeta, ComponentStory } from "@storybook/react";

// component
import Popover from "./index";

export default {
  title: 'Popover',
  component: Popover
} as ComponentMeta<typeof Popover>

const Template: ComponentStory<typeof Popover> = (args) => <Popover {...args} />;

const Default = Template.bind({});
Default.args = {
  onClick: () => {
    alert('Modal on');
  },
};

export { Default }
