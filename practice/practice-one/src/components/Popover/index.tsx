// css
import './popover.css'

// react
import { useState } from 'react';

// constants
import { SIZE } from '@/constants/enum';

// components
import Button from '@/components/Button/NormalButton';

interface IPopover {
  onClick: () => void;
  labelText: string;
  children: string;
  isOpen: boolean;
}

const Popover = ({
  onClick,
  labelText,
  isOpen,
  children
}: IPopover) => {
  const [isPopoverOpen, setIsPopoverOpen] = useState(false);

  const handleClick = () => {
    setIsPopoverOpen(false);
    onClick();
  };

  return (
    <>
      <Button label={labelText}
        onClick={() => setIsPopoverOpen(!isPopoverOpen)}
        size={SIZE.LARGE}
        typeButton='primary' />

      {isPopoverOpen && !isOpen && (
        <div className='popover' onClick={handleClick} >
          <a className='popover-item'> {children} </a>
        </div>
      )}
    </>
  )
}

export default Popover
