// icon
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faXmark } from '@fortawesome/free-solid-svg-icons';

// css
import './modal.css'

// react
import { ReactNode, useState } from 'react';

// constant
import { SIZE } from '@/constants/enum';

// component
import Button from '@/components/Button/NormalButton';

interface IModal {
  isOpen: boolean;
  title: string;
  btnNamePrimary: string;
  btnNameSecondary?: string;
  onClickBtnPrimary: () => void;
  onClickBtnSecondary?: () => void;
  onCloseModal: () => void;
  children: ReactNode;
}

const Modal = ({
  isOpen,
  title,
  btnNamePrimary,
  btnNameSecondary,
  onCloseModal,
  onClickBtnPrimary,
  onClickBtnSecondary,
  children }: IModal) => {

  const [closeModal, setCloseModal] = useState(!isOpen);

  const handleClose = () => {
    setCloseModal(true);
    onCloseModal();
  };

  return (
    <>
      {closeModal ? null : (
        <div className="modal-overlay d-flex d-flex-center">
          <div className='modal'>
            <div className="modal-header d-flex d-flex-space-between">
              <h3 className='modal-title'>{title}</h3>
              <div className="modal-close icon"
                onClick={() => {
                  handleClose();
                }}>
                <FontAwesomeIcon icon={faXmark} />
              </div>
            </div>
            <div className="modal-body d-flex">
              {children}

              {btnNamePrimary && (
                <Button
                  label={btnNamePrimary}
                  size={SIZE.SMALL}
                  typeButton='primary'
                  onClick={onClickBtnPrimary}
                />
              )}

              {btnNameSecondary && onClickBtnSecondary && (
                <Button
                  label={btnNameSecondary}
                  size={SIZE.SMALL}
                  onClick={onClickBtnSecondary}
                />
              )}
            </div>
          </div>
        </div>
      )}
    </>
  )
}

export default Modal;
