import { ComponentMeta, ComponentStory } from '@storybook/react';
import Input from '../Input';

import Modal from './index';

export default {
  title: 'Modal',
  component: Modal,
} as ComponentMeta<typeof Modal>;

const Template: ComponentStory<typeof Modal> = (args) => <Modal {...args} />;

const Default = Template.bind({});
Default.args = {
  isOpen: true,
  title: 'This is title',
};

const HasButton = Template.bind({});
HasButton.args = {
  isOpen: true,
  title: 'This is title',
  btnNamePrimary: 'Button 1',
  btnNameSecondary: 'Button 2',
};

export { Default, HasButton };
