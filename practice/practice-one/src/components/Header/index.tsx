import './header.css'

const Header = () => {
  return (
    <header className='header-wrapper'>
      <h1 className='header-title'> User Manager </h1>
    </header>
  )
}

export default Header;
