// css
import './edit.css'

// Types
import { IUser } from '@/types/users';

// react
import { useEffect, useState } from 'react';

// server and helper
import { updateUser, remove } from '@/services';
import { convertBase64 } from '@/helpers/converts';
import { SIZE } from '@/constants/enum';

// components
import Button from '@/components/Button/NormalButton';
import Input from '@/components/Input';
import InputFile from '@/components/Input/File';
import Avatar from '@/components/Avatar';
import SwitchButton from '@/components/Button/SwitchButton';
import Label from '@/components/Status';
import Modal from '@/components/Modal';
import { validateEmail, validateEmpty } from '@/helpers/validate';
import { INPUT_ERROR_MESSAGES } from '@/constants/message';

interface IEditProps {
  dataItem: IUser;
  registered: string;
  lastVisit: string;
  renderNewUsers: () => void
}

const EditUser = ({
  dataItem,
  registered,
  lastVisit,
  renderNewUsers
}: IEditProps) => {

  const [data, setData] = useState(dataItem);
  const [baseImage, setBaseImage] = useState('');
  const [stateModal, setStateModal] = useState(false);

  const isActive = data.status;

  useEffect(() => {
    setData(dataItem)
  }, [dataItem.id])

  // Handle delete user
  const handleDelete = () => {
    setStateModal(true);
  }

  // Confirm Remove user
  const confirmRemoveUser = async () => {
    await remove(data.id);
    setStateModal(false);

    renderNewUsers();
  }

  // Close modal
  const closeModalDelete = () => {
    setStateModal(false);
  };

  // Handle update user
  const handleUpdate = async () => {

    // Validate Input Empty
    const errorMessage = validateEmpty(data.name, INPUT_ERROR_MESSAGES.INPUT_EMPTY);

    if (errorMessage) {
      alert(errorMessage);

      return;
    }

    // Validate the email
    if (!validateEmail(data.email)) {
      alert(INPUT_ERROR_MESSAGES.EMAIL_INVALID);
      return;
    }

    const update = {
      ...data,
      lastUpdate: new Date(),
      avatar: data.avatar || baseImage
    };

    await updateUser(data.id, update);
    renderNewUsers();
  }

  // Handle change status
  const handleChangeCheckbox = () => {
    setData(prev => ({
      ...prev,
      status: !prev.status
    }));
  }

  // Handle change input
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setData(prev => ({
      ...prev,
      [name]: value
    }));
  }

  // Handle Change avatar
  const handleFileInputChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const files = event.target.files
    const file = files && files[0];
    const base64 = await convertBase64(file!) as string;

    setBaseImage(base64)
  };

  return (
    <>
      <div className='edit'>
        <div className='edit-wrapper d-flex'>
          <Button
            label='Delete'
            onClick={handleDelete}
            size={SIZE.SMALL}
          />
          <Button
            label='Save'
            onClick={handleUpdate}
            size={SIZE.SMALL}
            typeButton='primary'
          />
        </div>
        <div className="mg-top-m">
          <Input
            label='Full Name'
            size={SIZE.MEDIUM}
            name='name'
            value={data.name}
            onChange={handleInputChange} />
        </div>
        <div className="mg-top-m">
          <Input
            label='Email'
            size={SIZE.MEDIUM}
            name='email'
            value={data.email}
            onChange={handleInputChange} />
        </div>
        <div className="edit-upload d-flex mg-top-m">
          <InputFile
            label='Avatar'
            labelText='Upload New Photo'
            name='fileAvatar'
            handleChange={handleFileInputChange}
            htmlFor='avatar'
          />
          <Avatar
            name={data.name}
            avatar={data.avatar || baseImage}
            size={SIZE.MEDIUM} />
        </div>

        <div className='edit-label d-flex mg-top-m'>
          <p className="edit-title">Status:</p>
          <SwitchButton isChecked={isActive} onChange={handleChangeCheckbox} />
          <Label isActive={isActive} />
        </div>

        <div className='edit-label d-flex mg-top-m'>
          <p className="edit-title">Registered:</p>
          <p>{registered}</p>
        </div>
        <div className='edit-label d-flex mg-top-m'>
          <p className="edit-title">Last visited:</p>
          <p>{lastVisit}</p>
        </div>
      </div>

      {stateModal ? (
        <Modal
          isOpen={stateModal}
          title="Are you sure to delete this user?"
          onCloseModal={closeModalDelete}
          btnNamePrimary='Delete'
          btnNameSecondary='Cancel'
          onClickBtnPrimary={confirmRemoveUser}
          onClickBtnSecondary={closeModalDelete}>
        </Modal>
      ) : null}
    </>
  )

}

export default EditUser;
