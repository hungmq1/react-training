import Sidebar from "./index";

export default {
  title: 'Sidebar',
  component: Sidebar
}

export const SidebarStories = () => <Sidebar />;

