// icon
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserGroup } from '@fortawesome/free-solid-svg-icons';

// css
import './sidebar.css'

// react
import { useState } from 'react';

// components
import Popover from '@/components/Popover';
import Modal from '@/components/Modal';
import Input from '@/components/Input';
import { createUser } from '@/services';

// helper
import { validateEmpty } from '@/helpers/validate';

// constant
import { SIZE } from '@/constants/enum';
import { INPUT_ERROR_MESSAGES } from '@/constants/message';

interface IProps {
  onNewUserAdded: () => void
}

const Sidebar = ({ onNewUserAdded }: IProps) => {
  const [isPopoverOpen, setIsPopoverOpen] = useState(false);
  const [stateModal, setStateModal] = useState(false);
  const [inputValue, setInputValue] = useState('')

  // Handle change input
  const handleChangeText = (event: React.FormEvent<HTMLInputElement>) => {
    setInputValue(event.currentTarget.value);
  };

  // open modal when click
  const openModal = () => {
    setStateModal(true);
    setIsPopoverOpen(false);
  };

  // close modal when click
  const closeModal = () => {
    setInputValue('');
    setStateModal(false);
    setIsPopoverOpen(false);
  };

  // handle add new user
  const handleAddNewUser = async (username: string) => {
    const errorMessage = validateEmpty(username, INPUT_ERROR_MESSAGES.INPUT_EMPTY);

    if (errorMessage) {
      alert(errorMessage);

      return;
    }

    const payload = {
      name: username,
      email: '',
      status: false,
      avatar: '',
      registered: new Date(),
      lastUpdate: new Date()
    };

    await createUser(payload);
    closeModal();
    // call re-render
    onNewUserAdded();
  };

  return (
    <>
      <aside className='sidebar'>
        <Popover
          isOpen={isPopoverOpen}
          children='Add New User'
          labelText='New'
          onClick={openModal} />

        <div className="sidebar-item sidebar-active"><FontAwesomeIcon icon={faUserGroup} /> Users</div>
      </aside>

      {stateModal ? (
        <Modal
          isOpen={stateModal}
          title="Enter user name"
          onCloseModal={closeModal}
          btnNamePrimary='Save'
          onClickBtnPrimary={() => { handleAddNewUser(inputValue) }}>
          <Input
            size={SIZE.MEDIUM}
            value={inputValue}
            onChange={handleChangeText}
          />
        </Modal>
      ) : null}
    </>
  )
}
export default Sidebar;
