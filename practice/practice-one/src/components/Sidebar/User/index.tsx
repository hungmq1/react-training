// react
import moment from 'moment';
import { useState } from 'react';

// constant
import { FORMAT } from '@/constants/enum';

// css
import './sidebarUser.css'

// icon
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen } from '@fortawesome/free-solid-svg-icons';

// Types
import { IUser } from '@/types/users';

// Components
import ViewUser from '@/components/VIewUserInfo';
import Toolbar from '@/components/Toolbar';
import EditUser from '@/components/EditUser';

interface IProps {
  item: IUser;
  onNewUserAdded: () => void
}

const User = ({
  item,
  onNewUserAdded
}: IProps) => {
  const [editOpened, setEditOpened] = useState(false)

  const emailFormat = item.email || FORMAT.UNKNOWN;
  const registeredFormat = moment(item.registered).format(FORMAT.DATE);
  const lastVisitFormat = moment(item.lastUpdate).format(FORMAT.DATE);

  //On Edit
  const onClickEdit = () => {
    setEditOpened(true);
  };

  //Off Edit
  const closeFormEdit = () => {
    setEditOpened(false);
  };

  const renderUserInfo = (data: IUser) => {
    const { name, avatar } = data;

    return (
      <ViewUser
        username={name}
        avatar={avatar}
        email={emailFormat}
        lastDate={lastVisitFormat}
      />
    );
  };

  const renderUserEdit = (data: IUser) => {
    return (
      <EditUser
        dataItem={data}
        registered={registeredFormat}
        lastVisit={lastVisitFormat}
        renderNewUsers={onNewUserAdded}
      />
    );
  };

  return (
    <div className='sidebar-wrapper'>
      <Toolbar
        title='User information'
        hasStatus={true}
        isActive={item.status}
        hasCancel={editOpened}
        onCloseEdit={closeFormEdit}
        onClick={onClickEdit}
        icon={<FontAwesomeIcon icon={faPen} />} />
      {!editOpened ? renderUserInfo(item) : renderUserEdit(item)}
    </div>
  );
};

export default User;
