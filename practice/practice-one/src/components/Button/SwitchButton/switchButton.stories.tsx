import { ComponentMeta, ComponentStory } from '@storybook/react';

import SwitchButton from './index';

export default {
  title: 'Button/SwitchButton',
  component: SwitchButton,
} as ComponentMeta<typeof SwitchButton>;

const Template: ComponentStory<typeof SwitchButton> = (args) => (<SwitchButton {...args} />);

const NotChecked = Template.bind({});
NotChecked.args = {
  isChecked: false,
};

const Checked = Template.bind({});
Checked.args = {
  isChecked: true,
};

export { NotChecked, Checked };
