import { SIZE } from '@/constants/enum';
import { ComponentMeta, ComponentStory } from '@storybook/react';

// component
import Button from './index'

export default {
  title: 'Button/NormalButton',
  component: Button
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

const AddButton = Template.bind({});
AddButton.args = {
  typeButton: 'primary',
  label: '+ New',
  size: SIZE.LARGE,
  onClick: () => {
    alert('Add Button');
  },
};

const SecondaryButton = Template.bind({});
SecondaryButton.args = {
  label: 'Secondary',
  size: SIZE.SMALL,
  onClick: () => {
    alert('Secondary Button');
  },
};

const PrimaryButton = Template.bind({});
PrimaryButton.args = {
  typeButton: 'primary',
  label: 'Primary',
  size: SIZE.SMALL,
  onClick: () => {
    alert('Primary Button');
  },
};

export { AddButton, SecondaryButton, PrimaryButton };
