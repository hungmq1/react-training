// constant
import { SIZE } from '@/constants/enum';

// react
import clsx from 'clsx';

// css
import './button.css'

interface ButtonProps {
  typeButton?: 'primary' | 'secondary';
  label: string;
  size?: SIZE.SMALL | SIZE.MEDIUM | SIZE.LARGE;
  onClick: () => void;
  disabled?: boolean;
}

const Button = ({
  typeButton,
  label,
  size,
  onClick,
  disabled }: ButtonProps) => {

  return (
    <div>
      <button
        className={clsx('btn', typeButton && `btn-${typeButton}`, size && `btn-${size}`)}
        onClick={onClick}
        disabled={disabled}>
        {label}
      </button>
    </div>
  )
}

export default Button;
