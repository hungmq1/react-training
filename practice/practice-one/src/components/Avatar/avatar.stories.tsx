import { SIZE } from "@/constants/enum";
import { ComponentMeta, ComponentStory } from "@storybook/react";
import Avatar from "./index";

export default {
  title: 'Avatar',
  component: Avatar
} as ComponentMeta<typeof Avatar>;

const Template: ComponentStory<typeof Avatar> = (args) => <Avatar {...args} />;

const AvatarCircle = Template.bind({});
AvatarCircle.args = {
  name: 'name',
  size: SIZE.SMALL,
  circle: true,
};

const Small = Template.bind({});
Small.args = {
  name: 'name',
  size: SIZE.SMALL,
};

const Medium = Template.bind({});
Medium.args = {
  name: 'name',
  size: SIZE.MEDIUM,
};

const Large = Template.bind({});
Large.args = {
  name: 'name',
  size: SIZE.LARGE,
};

const Image = Template.bind({});
Image.args = {
  avatar: 'https://i.pinimg.com/236x/98/a3/c5/98a3c583ff9bc2efee03061f74d538ca--himoto-umaru-chan-umaru-chan-wallpaper.jpg',
  size: SIZE.MEDIUM,
  circle: true,
};

export { AvatarCircle, Small, Medium, Large, Image }
