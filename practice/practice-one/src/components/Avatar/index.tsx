// react
import clsx from 'clsx';

// constant
import { SIZE } from '@/constants/enum';

// CSS
import './avatar.css';

interface IProps {
  avatar?: string;
  name: string;
  circle?: boolean;
  size?: SIZE.SMALL | SIZE.MEDIUM | SIZE.LARGE;
}

const Avatar = ({
  avatar,
  name,
  circle,
  size
}: IProps) => {

  const nameAvatar = name ? name.charAt(0).toUpperCase() : '';

  const renderImg = () => {
    return (
      <img
        src={avatar}
        alt={name}
        className={clsx('avatar', `avatar-${size}`, { 'avatar-circle': circle })}
      />
    )
  }

  const renderCircle = () => {
    return (
      <div className={clsx('avatar d-flex d-flex-center', `avatar-${size}`, { 'avatar-circle': circle })}>
        {nameAvatar}
      </div>
    )
  }

  return (
    <>
      {avatar ? renderImg() : renderCircle()}
    </>
  )
}

export default Avatar;
