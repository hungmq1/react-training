// css
import { ReactNode } from 'react';
import './tableBody.css'

interface IProps {
  children: ReactNode;
}

const TableBody= ({ children }: IProps) => {
  return (
    <tbody className='table-body'>
      {children}
    </tbody>
  );
};

export default TableBody;
