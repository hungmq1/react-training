import { ReactNode } from "react"

// css
import './tableCell.css'
interface IProps {
  children?: ReactNode,
  type?: ReactNode;
}
const TableCell = ({
  children,
  type
}: IProps) => {
  const TagName = type === 'th' ? 'th' : 'td';
  return (
    <TagName className="table-cell">
      {children}
    </TagName>
  )
}

export default TableCell
