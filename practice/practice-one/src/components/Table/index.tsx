import { ReactNode } from "react"

// css
import './table.css'

interface IProps {
  children: ReactNode
}
const Table = ({children}: IProps) => {
  return (
    <table className='table'>{children}</table>
  )
}

export default Table
