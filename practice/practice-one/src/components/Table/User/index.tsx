// components
import Table from '@/components/Table';
import TableCell from '@/components/Table/Cell';
import TableHead from '@/components/Table/Head';
import TableRow from '@/components/Table/Row'
import TableUserRow from '@/components/Table/User/Row'
import TableBody from '@/components/Table/Body';
import TableRowLoading from '@/components/Loading';
import TableRowEmpty from '@/components/Table/Row/TableRowEmpty';

// css
import './tableUser.css'

// Types
import { IUser } from '@/types/users'

interface IProps {
  data: IUser[];
  loading: boolean;
  handleClick: (user: IUser) => void;
}

const TableUser = ({ data, loading, handleClick }: IProps) => {

  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell type='th'></TableCell>
          <TableCell type='th'>Full Name</TableCell>
          <TableCell type='th'>Status</TableCell>
          <TableCell type='th'>Email</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {loading && (
          <TableRowLoading />
        )}

        {data.length === 0 ? (
          <TableRowEmpty message="No data available." />
        ) : (
          data.map((user) => (
            <TableUserRow key={user.id} data={user} handleClick={handleClick} />
          ))
        )}
      </TableBody>
    </Table>
  );
};


export default TableUser;
