// components
import Avatar from "@/components/Avatar";
import Label from "@/components/Status";
import TableRow from "@/components/Table/Row";
import TableCell from "@/components/Table/Cell";

// Types
import { IUser } from '@/types/users'

// constant
import { SIZE } from '@/constants/enum'

interface IProps {
  data: IUser;
  handleClick: (user: IUser) => void;
}

const TableUserRow = ({ data, handleClick }: IProps) => {
  return (
      <TableRow onClickRow={() => handleClick(data)}>
        <TableCell type='td'>
          <Avatar
            name={data.name}
            avatar={data.avatar}
            circle={true}
            size={SIZE.SMALL} />
        </TableCell>
        <TableCell type='td'>{data.name}</TableCell>
        <TableCell type='td'>
          <Label isActive={data.status} />
        </TableCell>
        <TableCell type='td'>{data.email}</TableCell>
      </TableRow>
  )
};

export default TableUserRow;
