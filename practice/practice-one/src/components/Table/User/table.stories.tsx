import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import TableUser from './index';

export default {
  title: 'TableUser',
  component: TableUser,
} as ComponentMeta<typeof TableUser>;

const Template: ComponentStory<typeof TableUser> = (args) => <TableUser {...args} />;

export const WithData = Template.bind({});
WithData.args = {
  data: [
    {
      id: 1,
      name: 'John Doe',
      status: true,
      email: 'john.doe@example.com',
      avatar: '',
      registered: new Date,
      lastUpdate: new Date
    },
    {
      id: 2,
      name: 'Inculi Nano',
      status: false,
      email: '',
      avatar: '',
      registered: new Date,
      lastUpdate: new Date
    },
  ],
  loading: false,
  handleClick: (user) => {
    alert(user.name);
  },
};

export const Loading = Template.bind({});
Loading.args = {
  data: [],
  loading: true,
  handleClick: (user) => {
    console.log(user);
  },
};

export const NoData = Template.bind({});
NoData.args = {
  data: [],
  loading: false,
  handleClick: (user) => {
    console.log(user);
  },
};
