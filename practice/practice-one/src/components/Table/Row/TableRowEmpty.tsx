import { ReactNode } from 'react'
import './tableRow.css'

interface IProps {
  message: ReactNode;
}

const TableRowEmpty = ({ message }: IProps) => {

  return (
    <tr className="table-row-empty">
      <td className="table-col-empty">{message}</td>
    </tr>
  )
}

export default TableRowEmpty
