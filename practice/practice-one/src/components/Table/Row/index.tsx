import { ReactNode } from "react";

// css
import './tableRow.css'
interface IProps {
  children: ReactNode;
  onClickRow?: (event: React.MouseEvent<HTMLTableRowElement>) => void;
}

const TableRow = ({
  children,
  onClickRow
}: IProps) => {

  return (
    <tr
      onClick={onClickRow}
      className='table-row' >
      {children}
    </tr>
  );
};

export default TableRow;
