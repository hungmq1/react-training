// css
import './viewUserInfo.css'

// Components
import Avatar from '@/components/Avatar';
import ViewInfo from './ViewInfo';

// icon
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClock, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { memo } from 'react';

// constants
import { SIZE } from '@/constants/enum';

interface IProps {
  username: string;
  avatar?: string;
  email?: string;
  lastDate?: string;
}

const ViewUser = ({
  username,
  avatar,
  email,
  lastDate
}: IProps) => {

  return (
    <div className="view">
      <div className="view-info">
        <div className='d-flex d-flex-center'>
          <Avatar
            size={SIZE.LARGE}
            avatar={avatar}
            name={username}
          />
        </div>
        <h3>{username}</h3>
      </div>
      <div className="view-body">
        <ViewInfo
          label="Email"
          icon={<FontAwesomeIcon icon={faEnvelope} />}>
          {email}
        </ViewInfo>
        <ViewInfo
          label="LastVisited"
          icon={<FontAwesomeIcon icon={faClock} />}>
          {lastDate}
        </ViewInfo>
      </div>
    </div>
  )
};

export default memo(ViewUser);
