import { ComponentMeta, ComponentStory } from '@storybook/react';

import ViewUser from './index';

export default {
  title: 'ViewUser',
  component: ViewUser,
} as ComponentMeta<typeof ViewUser>;

const Template: ComponentStory<typeof ViewUser> = (args) => <ViewUser {...args} />;

const Default = Template.bind({});
Default.args = {
  username : 'username',
  avatar: '',
  email: 'mail@gmail.com',
  lastDate: '2023-02-22'
};

export { Default };
