import { faBrain } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ComponentMeta, ComponentStory } from '@storybook/react';

import ViewInfo from './index';

export default {
  title: 'ViewInfo',
  component: ViewInfo,
} as ComponentMeta<typeof ViewInfo>;

const Template: ComponentStory<typeof ViewInfo> = (args) => <ViewInfo {...args} />;

const Default = Template.bind({});
Default.args = {
  label: 'Name Label',
  icon: <FontAwesomeIcon icon={faBrain}/>,
  children: 'This is the description'
};

export { Default };
