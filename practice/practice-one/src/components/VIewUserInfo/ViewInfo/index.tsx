import { ReactNode } from 'react';
import './viewInfo.css'

interface IViewInfoProps {
  label: string;
  icon?: ReactNode;
  children?: ReactNode;
}

const ViewInfo = ({
  label,
  icon,
  children
}: IViewInfoProps) => {

  return (
    <div>
      <div className="info">
        <div className="info-wrapper d-flex">
          <div className="info-icon">
            {icon}
          </div>
          <p>{label}:</p>
        </div>
        <div className="info-children">
          {children}
        </div>

      </div>
    </div>
  )
}

export default ViewInfo
