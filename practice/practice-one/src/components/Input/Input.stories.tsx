import { ComponentMeta, ComponentStory } from '@storybook/react';

import Input from "./index";

export default {
  title: 'Input',
  component: Input,

} as ComponentMeta<typeof Input>;

const Template: ComponentStory<typeof Input> = (args) => <Input {...args} />;

const Default = Template.bind({});
Default.args = {
  label: 'form input',
  name: 'Name',
  value: ''
};

export { Default };
