// css
import './input.css'

// constant
import { SIZE } from '@/constants/enum';

// react
import clsx from 'clsx';

interface InputProps {
  label?: string;
  name?: string;
  value?: string;
  type?: string;
  size?: SIZE.SMALL | SIZE.MEDIUM | SIZE.LARGE;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const Input = ({
  label,
  name,
  value,
  type,
  size,
  onChange
}: InputProps) => {

  return (
    <div className="input-box d-flex">
      {label && <p className="input-label">{label}:</p>}
      <input
        name={name}
        value={value}
        onChange={onChange}
        type={type}
        className={clsx('input-form', `input-${size}`)} />
    </div>

  );
};

export default Input;
