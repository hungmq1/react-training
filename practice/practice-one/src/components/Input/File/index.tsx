// css
import './inputFile.css'

import { ChangeEvent } from 'react';

// Font Awesome
import { faFileArrowUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

interface IProps {
  label: string;
  labelText: string;
  name: string;
  htmlFor: string;
  handleChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

const InputFile = ({
  label,
  labelText,
  name,
  htmlFor,
  handleChange
}: IProps) => {

  return (
    <div className='input-file d-flex d-flex-center'>
      <p className='input-file-label'>{label}:</p>
      <label htmlFor={htmlFor} className='input-file-upload d-flex d-flex-center'>
        <FontAwesomeIcon icon={faFileArrowUp} className='input-file-upload-icon' />
        {labelText}
      </label>
      <input id={htmlFor} type='file' name={name} onChange={handleChange} />
    </div>
  );
};

export default InputFile;
