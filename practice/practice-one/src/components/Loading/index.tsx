// css
import './loading.css'

const Loading = () => {
  return (
    <tr>
      <td className='loading-cell'>
        <div className='loading-overlay'>
          <div className='loading-spinner'></div>
        </div>
      </td>
    </tr>
  )
}

export default Loading;
