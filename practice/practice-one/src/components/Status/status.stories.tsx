import { ComponentMeta, ComponentStory } from '@storybook/react';

import Status from './index';

export default {
  title: 'Status',
  component: Status,
} as ComponentMeta<typeof Status>;

const Template: ComponentStory<typeof Status> = (args) => <Status {...args} />;

const NotActive = Template.bind({});
NotActive.args = {
  isActive: false,
};

const Active = Template.bind({});
Active.args = {
  isActive: true,
};

export { NotActive, Active };
