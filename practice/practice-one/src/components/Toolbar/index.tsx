// css
import './toolbar.css';

// react
import { ReactNode } from 'react';

// icon
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

// components
import Label from '@/components/Status';

interface IProps {
  hasStatus?: boolean;
  hasCancel?: boolean;
  isActive?: boolean;
  title: string;
  icon: ReactNode;
  onClick?: () => void;
  onCloseEdit?: () => void;
}

const Toolbar = ({
  hasStatus,
  hasCancel,
  isActive,
  title,
  icon,
  onClick,
  onCloseEdit }: IProps) => {

  const renderToolBarEdit = () => (
    <div className='icon' onClick={onCloseEdit}>
      <FontAwesomeIcon icon={faArrowLeft} />
    </div>
  )

  const isShowLabel = !hasCancel;

  return (
    <div className='toolbar d-flex d-flex-space-between'>
      <div className='d-flex toolbar-wrapper'>
        {hasCancel && renderToolBarEdit()}
        <h2 className='toolbar-title'>{title}</h2>
      </div>
      {isShowLabel && (
        <div className="toolbar-wrapper d-flex d-flex-space-between">
          {hasStatus && <Label isActive={isActive!} />}
          <div className="icon" onClick={onClick}>
            {icon}
          </div>
        </div>
      )}
    </div>
  )
};

export default Toolbar;
