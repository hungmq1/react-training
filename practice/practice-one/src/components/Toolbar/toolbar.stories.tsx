import { faPen , faSearch} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { ComponentMeta, ComponentStory } from '@storybook/react';

import Toolbar from './index';

export default {
  title: 'Toolbar',
  component: Toolbar,
} as ComponentMeta<typeof Toolbar>;

const Template: ComponentStory<typeof Toolbar> = (args) => <Toolbar {...args} />;

const Default = Template.bind({});
Default.args = {
  title: 'Name',
  icon: <FontAwesomeIcon icon={faSearch}/>
};

const HasStatus = Template.bind({});
HasStatus.args = {
  title: 'Name',
  hasStatus: true,
  icon: <FontAwesomeIcon icon={faPen}/>
};

export { Default, HasStatus };
