// css
import './searchBar.css'

// constant
import { SIZE } from '@/constants/enum';

// icon
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faXmark } from '@fortawesome/free-solid-svg-icons';

// components
import Input from '@/components/Input';

interface IProps {
  inputValue: string | undefined;
  onClose?: () => void;
  onSearch?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const SearchBar = ({
  inputValue,
  onClose ,
  onSearch
}: IProps) => {

  // Change input
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    onSearch!(event);
  };

  return (
    <div className='search d-flex'>
      <Input
        onChange={handleInputChange!}
        size={SIZE.LARGE}
        value={inputValue} />

      <div className="icon" onClick={onClose}>
        <FontAwesomeIcon icon={faXmark} />
      </div>
    </div>
  )
};

export default SearchBar;
