import { ComponentMeta, ComponentStory } from '@storybook/react';

import SearchBar from './index';

export default {
  title: 'SearchBar',
  component: SearchBar,
} as ComponentMeta<typeof SearchBar>;

const Template: ComponentStory<typeof SearchBar> = (args) => <SearchBar {...args} />;

const Default = Template.bind({});

export { Default };
