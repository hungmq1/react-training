// components
import Sidebar from '@/components/Sidebar';
import SidebarUser from '@/components/Sidebar/User';
import Toolbar from '@/components/Toolbar';
import { getUsers } from '@/services'
import TableUser from '@/components/Table/User';
import SearchBar from '@/components/SearchBar';

// icon
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

// css
import './home.css'

// Type
import { IUser } from '@/types/users'
import { useEffect, useState } from 'react';
import { API_ERROR_MESSAGES } from '@/constants/message';

const HomePage = () => {
  const [data, setData] = useState<IUser[]>([])
  const [showSidebarUser, setShowSidebarUser] = useState<boolean>(false);
  const [selectedUser, setSelectedUser] = useState<IUser | null>(null);
  const [dataChanged, setDataChanged] = useState<boolean>(false);
  const [showToolBar, setShowToolBar] = useState(true);
  const [showSearchBar, setShowSearchBar] = useState(false);
  const [searchValue, setSearchValue] = useState<string>('');
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getDataUsers()
  }, [dataChanged])

  //Get data list user
  const getDataUsers = async () => {
    setIsLoading(true);

    const users = await getUsers();

    setTimeout(() => {
      setIsLoading(false)
    }, 1000);

    if (users.error) {
      alert(API_ERROR_MESSAGES.GET_API);
    }

    setData(users?.data || [])
  }

  // Handle re-render add new user
  const handleRenderNewUser = () => {
    setDataChanged(!dataChanged);
  };

  // Handle click row data
  const handleUserSelected = (user: IUser) => {
    setSelectedUser(user);
    setShowSidebarUser(true);
  };

  // On click open search bar
  const onOpenSearchBar = () => {
    setShowSearchBar(true);
    setShowToolBar(false)
  }

  // On click open search bar
  const onCloseSearchBar = () => {
    setShowSearchBar(false);
    setShowToolBar(true);
    setSearchValue('');
  }

  // Handle Search
  const handleSearchUser = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    const { value } = event.target;
    setSearchValue(value);
  };

  // Filter user
  const filterData = data.filter((item) => {
    return searchValue.toLowerCase() === '' ? item : item.name.toLowerCase().includes(searchValue);
  });

  return (
    <div className='d-flex'>
      <Sidebar onNewUserAdded={handleRenderNewUser} />

      <div className='home-wrapper'>
        {showToolBar && (
          <Toolbar
            title='Users'
            icon={<FontAwesomeIcon icon={faSearch} />}
            onClick={onOpenSearchBar} />
        )}

        {showSearchBar && (
          <SearchBar
            onClose={onCloseSearchBar}
            inputValue={searchValue}
            onSearch={handleSearchUser}
          />
        )}

        <TableUser
          data={filterData}
          loading={isLoading}
          handleClick={handleUserSelected} />

      </div>

      {showSidebarUser && selectedUser && (
        <SidebarUser
          item={selectedUser}
          onNewUserAdded={handleRenderNewUser} />
      )}
    </div>
  )
}

export default HomePage;
