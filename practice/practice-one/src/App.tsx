// Css
import './App.css'

// Components
import Header from '@/components/Header'
import HomePage from '@/Pages/Home'

function App() {

  return (
    <div className="App">
      <Header />
      <HomePage/>
    </div>
  )
}

export default App
