interface IUser {
  id: number;
  name: string;
  avatar: string;
  email: string;
  status: boolean;
  registered: Date;
  lastUpdate: Date;
}

export type { IUser }
