/**
 * Convert file image to base64
 * @param {Blob} file
 * @returns {ImageData/undefined}
 */
const convertBase64 = (file: Blob) => {

  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  })
};

export { convertBase64 }
