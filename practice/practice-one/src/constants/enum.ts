enum STATUS {
  ACTIVE = 'Active',
  NOT_ACTIVE = 'Not active'
}

enum SIZE {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
}

enum FORMAT {
DATE = 'MMM DD, YYYY hh:mm:ss',
UNKNOWN = 'Unknown'
}
export { STATUS, SIZE, FORMAT }
