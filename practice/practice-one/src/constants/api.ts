const URL_API = 'http://localhost:3000';
const STATUS_CODE = {
  OK: 200
};

export {
  URL_API,
  STATUS_CODE
};
