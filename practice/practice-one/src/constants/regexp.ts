const REGEXP_EMAIL = /^[a-zA-Z0-9.]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

export { REGEXP_EMAIL };
