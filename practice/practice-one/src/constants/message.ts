// Message API
const API_ERROR_MESSAGES = {
  GET_API: 'Failed to retrieve user from API',
  ADD_USER: 'Failed to add new user to API',
  UPDATE_USER: 'Failed to update user',
  DELETE_USER: 'Failed to delete user',
};

// Message input error
const INPUT_ERROR_MESSAGES = {
  EMAIL_INVALID: 'Invalid email address',
  INPUT_EMPTY: 'Input cannot be empty',
};

export {
  API_ERROR_MESSAGES,
  INPUT_ERROR_MESSAGES
}
