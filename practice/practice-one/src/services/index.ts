import axios from 'axios';

// constant
import { URL_API } from '@/constants/api';

// interface
import { IUser } from '@/types/users';

axios.defaults.baseURL = URL_API;

/**
 * Get users from json-server
 * @returns {Object}
 */
const getUsers = async () => {
  try {
    const response = await axios.get(`/users`);

    return {
      data: response.data,
      error: null,
    };
  } catch (error) {

    return {
      data: null,
      error,
    };
  }
};

/**
 * @param {IUser} payload
 * @returns {Object}
 */
const createUser = async (payload: Omit<IUser, 'id'>) => {
  try {
    const response = await axios.post('/users', payload);

    return {
      data: response.data,
      error: null,
    };
  } catch (error) {

    return {
      data: null,
      error,
    };
  }
};

/**
 * Update user
 * @param {String| Number} id
 * @param {IUser} payload
 * @returns {Object}
 */
const updateUser = async (id: string | number, payload: IUser) => {
  try {
    const response = await axios.patch(`/users/${id}`, payload);

    return {
      data: response.data,
      error: null,
    };
  } catch (error) {

    return {
      data: null,
      error,
    };
  }
};

/**
 * Remove user by id
 * @param {Number} id - id of user
 *
 * @returns {Object}
 */
const remove = async (id: number) => {
  try {
    const response = await axios.delete(`/users/${id}`);

    return {
      data: response.data,
      error: null,
    };
  } catch (error) {

    return {
      data: null,
      error,
    };
  }
};
export { getUsers, createUser, updateUser, remove }
