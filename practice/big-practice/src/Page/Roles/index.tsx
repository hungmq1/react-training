import { useMemo, useState } from 'react';

// components
import Appbar from '@/components/Appbar';
import SearchBar from '@/components/SearchBar';
import RoleSidebar from './RoleSidebar';
import TableRole from './TableRole';

// FontAwesome
import { faSearch } from '@fortawesome/free-solid-svg-icons';

// constants
import { API_ERROR_MESSAGES } from '@/constants/message';

// types
import { IRole } from '@/types/roles';

// css
import '@/Page/page.css';

// helper
import { filterData } from '@/helpers/search';

// hooks
import { useRoles, useRules, useUsers } from '@/hooks';

const RoleManager = () => {

  const [showSidebarRole, setShowSidebarRole] = useState<boolean>(false);
  const [selectedRole, setSelectedRole] = useState<IRole | null>(null);
  const [hidden, setHidden] = useState(false);
  const [searchValue, setSearchValue] = useState('');

  // Fetch API
  const { data: rolesData, error: rolesError } = useRoles();
  const { data: usersData, error: usersError } = useUsers();
  const { data: rulesData, error: rulesError } = useRules();

  if (rolesError || usersError || rulesError) {
    alert(API_ERROR_MESSAGES.GET_API);
  };

  // Handle click row data
  const handleRoleSelected = (role: IRole) => {
    setSelectedRole(role)
    setShowSidebarRole(true);
  };

  // Search filter roles
  const filterRole = useMemo(() => {
    return filterData<IRole>(rolesData?.list, searchValue);
  }, [rolesData, searchValue]);

  // close searchBar
  const onCloseSearchBar = () => {
    setHidden(false)
    setSearchValue('');
  };

  // Change AppBar
  const onChangeAppBar = () => {
    setHidden(!hidden)
  };

  // change input search
  const handleSearchRole = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();

    const { value } = event.target;
    setSearchValue(value);
  };

  return (
    <div className="d-flex">
      <div className="page-table">
        <div hidden={hidden}>
          <Appbar
            title="Role information"
            icon={faSearch}
            isIcon
            itemClick={onChangeAppBar}
          />
        </div>
        <div hidden={!hidden}>
          <SearchBar
            inputValue={searchValue}
            onClose={onCloseSearchBar}
            onSearch={handleSearchRole}
          />
        </div>

        <TableRole data={filterRole} onRowClick={handleRoleSelected} />
      </div>
      {showSidebarRole && selectedRole && (
        <RoleSidebar
          item={selectedRole}
          rules={rulesData?.list}
          users={usersData?.list}
          stateSidebar={setShowSidebarRole} />
      )}
    </div>
  )
};

export default RoleManager;
