// library
import { memo, useEffect, useState, Dispatch } from 'react'
import { mutate } from 'swr';

// layouts
import FormView from '@/layouts/View'
import EditInfo from '@/layouts/Edit/EditInfo';
import EditRules from '@/layouts/Edit/Rules';
import EditCheckBox from '@/layouts/Edit/CheckBox';

//types
import { IRole } from '@/types/roles';
import { IUser } from '@/types/users';
import { IRule } from '@/types/rules';

// constants
import { SIZE } from '@/constants/enum';
import { API_ERROR_MESSAGES, INPUT_ERROR_MESSAGES } from '@/constants/message';
import { listTabRoles } from '@/constants/listItem';

// helper
import { validateEmpty } from '@/helpers/validate';
import { findById } from '@/helpers/find';

// Font Awesome
import { faArrowLeft, faListCheck, faPen, faUserGroup } from '@fortawesome/free-solid-svg-icons';

// css
import '@/Page/page.css'

// service
import { removeRole, updateRole } from '@/services/role';
import { updateUser } from '@/services/user';

// components
import Appbar from '@/components/Appbar';
import IconButton from '@/components/Button/IconButton';
import Tabs from '@/components/Tabs';
import TabPanel from '@/components/Tabs/Panel'
import Input from '@/components/Input';
import ColorPicker from '@/components/ColorPicker';
import Modal from '@/components/Modal';

// hooks
import { useRolesStore } from '@/hooks';
import { formatItem } from '@/helpers/format';

interface IProps {
  item: IRole;
  users: IUser[];
  rules: IRule[];
  stateSidebar: Dispatch<React.SetStateAction<boolean>>;
}

const RoleSidebar = ({
  item,
  users,
  rules,
  stateSidebar
}: IProps) => {

  const [hidden, setHidden] = useState(false);
  const [value, setValue] = useState<string>('1');
  const [data, setData] = useState<IRole>(item);
  const [color, setColor] = useState<string>(data.color);
  const [openModal, setOpenModal] = useState(false);
  const { handleUpdateRole, handleDeleteRole } = useRolesStore();

  useEffect(() => {
    setData(item);
  }, [item]);

  // handle change tab
  const handleChange = (event: React.MouseEvent<HTMLButtonElement>) => {
    setValue(event.currentTarget.value);
  };

  // hande Update Role
  const handleSave = async () => {

    // Validate Input Empty
    const errorMessage = validateEmpty(data.name, INPUT_ERROR_MESSAGES.INPUT_EMPTY);

    if (errorMessage) {
      alert(errorMessage);
      return;
    }

    const update = {
      ...data,
      color
    };
    const roleUpdate = await updateRole(data.id, update);

    handleUpdateRole(roleUpdate.list);

    // update the role data with mutate
    mutate('/roles');

    if (roleUpdate.error) {
      alert(API_ERROR_MESSAGES.UPDATE)
      return;
    }

  };

  // hande Open Modal Delete
  const handleOpenModal = () => {
    setOpenModal(true)
  }

  // handle Close Modal Delete
  const handeCloseModal = () => {
    setOpenModal(false)
  }

  // handle Remove Role
  const handleRemoveRole = async () => {
    const remove = await removeRole(data.id);

    handleDeleteRole(data);

    // update the role data with mutate
    mutate('/roles');

    if (remove.error) {
      alert(API_ERROR_MESSAGES.DELETE);
      return;
    }

    stateSidebar(false);
    setOpenModal(false);
  };

  const handleColorChange = (newColor: string) => {
    setColor(newColor);
  };

  // Handle change input
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setData(prev => ({
      ...prev,
      [name]: value
    }));
  };

  // change AppBar
  const onChangeAppBar = () => {
    setHidden(!hidden)
  }

  // close TabBar
  const onCloseTabBar = () => {
    setHidden(false)
  }

  // handle  Checkbox rules
  const handleCheckRule = async (id: string, checked: boolean) => {
    const convertId = Number(id);
    const ruleActive = item.rules;

    if (ruleActive) {

      if (checked) {
        ruleActive.push(convertId);
        ruleActive.sort();
      } else {
        ruleActive.splice(ruleActive.indexOf(convertId), 1);
      }
    }

    const updateRules = {
      ...item,
      rules: ruleActive
    };

    const editRulesRole = await updateRole(updateRules.id, updateRules);

    handleUpdateRole(editRulesRole.list);
  };

  const handleCheckUser = async (id: string, checked: boolean) => {
    const convertId = Number(id);

    // check if users exist
    const userRoles = findById<IUser>(users, convertId);

    // set default
    let rolesUser: number[] = [];

    if (userRoles?.roles) {
      rolesUser = userRoles.roles;
    }

    const listUser = item.users;

    if (listUser) {

      if (checked) {
        listUser.push(convertId);
        listUser.sort();

        if (rolesUser) {
          rolesUser.push(item.id);
        }
      } else {
        listUser.splice(listUser.indexOf(convertId), 1);

        if (rolesUser[convertId]) {
          delete rolesUser[convertId]
        }
      }
    }

    // check if users exist and update data
    if (userRoles) {
      const updateMembers = {
        ...userRoles,
        roles: rolesUser
      };

      const editMembers = await updateUser(updateMembers.id, updateMembers);

      handleUpdateRole(editMembers.list);
    }
  }

  // set Rules active in roles
  const rulesActive = item.rules!.map((rule) => findById<IRule>(rules, rule)!);

  // set members active in roles
  const membersActive = item.users!.map((user) => findById<IUser>(users, user)!);

  const listRulesActive = item.rules
  ? formatItem(
    item.rules.map((rule) => findById<IRule>(rules, rule)!),
    'id',
    'description',
  ) : [];

// list rules Assigned form view
const listMembersActive = item.users
  ? formatItem(
    item.users.map((user) => findById<IUser>(users, user)!),
    'id',
    'name',
  ) : [];

  // render Form view
  const renderFormView = ({
    name
  }: IRole) => {

    return (
      <>
        <FormView
          hasAvatar
          name={name}
          color={data.color}
          rules={{
            title: 'Rules assigned',
            icon: faListCheck,
            listItem: listRulesActive.map(item => ({ id: item.id!, value: item.value })),
            count: item.rules?.length
          }}
          users={{
            title: 'Members assigned',
            icon: faUserGroup,
            listItem: listMembersActive.map(item => ({ id: item.id!, value: item.value })),
            count: item.users?.length
          }}
        />
      </>
    )
  };

  // render Tab edit General
  const renderTabPanel = () => {
    return (
      <>
        <TabPanel index={value} value={'1'}>
          <EditInfo
            btnTextPrimary='Save'
            btnTextSecondary='Delete'
            onClickPrimary={handleSave}
            onClickSecondary={handleOpenModal}>
            <div className="spacing-normal">
              <Input
                size={SIZE.SMALL}
                label='Name'
                name='name'
                value={data.name}
                onChange={handleInputChange} />
            </div>
            <div className="spacing-normal">
              <ColorPicker defaultColor={color} onColorChange={handleColorChange} />
            </div>
          </EditInfo>
        </TabPanel>
        <TabPanel index={value} value={'2'}>
          <EditRules
            name={item.name}
            rules={rules}
            rulesActive={rulesActive}
            onChange={handleCheckRule}
          />
        </TabPanel>
        <TabPanel index={value} value={'3'}>
          <TabPanel index={value} value={'3'}>
            <EditCheckBox
              name={item.name}
              list={users}
              listActive={membersActive}
              type='members'
              onChange={handleCheckUser}
            />
          </TabPanel>
        </TabPanel>
      </>
    )
  };

  return (
    <>
      <div hidden={hidden} className="page-sidebar page-view">
        <Appbar
          title="Roles information"
          icon={faPen}
          isIcon
          itemClick={onChangeAppBar} />
        {renderFormView(item)}
      </div>
      <div hidden={!hidden} className="page-sidebar page-edit">
        <div className="d-flex page-edit-header">
          <IconButton
            icon={faArrowLeft}
            onClick={onCloseTabBar}
          />
          <Tabs
            list={listTabRoles}
            index={value}
            onClick={handleChange}
          />
        </div>
        {renderTabPanel()}
      </div>

      {openModal && (
        <Modal
          isOpen={openModal}
          title="Are you sure to delete this role?"
          onCloseModal={handeCloseModal}
          btnNamePrimary='Delete'
          btnNameSecondary='Cancel'
          onClickBtnPrimary={handleRemoveRole}
          onClickBtnSecondary={handeCloseModal}>
        </Modal>
      )}
    </>
  )
};

export default memo(RoleSidebar);
