
import { ComponentMeta, ComponentStory } from '@storybook/react';

// components
import TableRole from './TableRole';

export default {
  title: 'TableRole',
  component: TableRole,
} as ComponentMeta<typeof TableRole>;

const Template: ComponentStory<typeof TableRole> = (args) => <TableRole {...args} />;

export const HasData = Template.bind({});
HasData.args = {
  data: [
    {
      id: 1,
      name: 'Admin',
      color: 'green'
    },
    {
      id: 2,
      name: 'Manager',
      color: 'orange'
    },
  ],
  onRowClick: (role) => {
    alert(role.name);
  },
};

export const NoData = Template.bind({});
NoData.args = {
  data: [],
  onRowClick: (user) => {
    console.log(user);
  },
};
