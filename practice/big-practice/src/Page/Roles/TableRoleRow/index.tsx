// components
import Avatar from '@/components/Avatar';
import TableRow from '@/components/Table/Row';
import TableCell from '@/components/Table/Cell';

// Types
import { IRole } from '@/types/roles'

// constant
import { SIZE } from '@/constants/enum'

interface IProps {
  data: IRole;
  isActive?: boolean;
  onRowClick: (role: IRole) => void;
}

const TableRoleRow = ({
  data,
  isActive = false,
  onRowClick
}: IProps) => {

  // handle row click
  const handleRowClick = () => {
    if (onRowClick) {
      onRowClick(data);
    }
  };

  return (
    <TableRow
      isActive={isActive}
      onRowClick={handleRowClick}
    >
      <TableCell size={SIZE.SMALL} type="td">
        <Avatar
          name={data.name}
          color={data.color}
          circle={true}
          size={SIZE.SMALL} />
      </TableCell>
      <TableCell size={SIZE.LARGE} type="td">{data.name}</TableCell>
    </TableRow>
  )
};

export default TableRoleRow;
