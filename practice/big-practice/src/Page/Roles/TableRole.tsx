import { memo, useState } from 'react';

// components
import Table from '@/components/Table';
import TableCell from '@/components/Table/Cell';
import TableHead from '@/components/Table/Head';
import TableRow from '@/components/Table/Row'
import TableRowEmpty from '@/components/Table/Row/Empty';
import TableBody from '@/components/Table/Body';
import TableRoleRow from '@/Page/Roles/TableRoleRow';

// Types
import { IRole } from '@/types/roles'

// constants
import { SIZE } from '@/constants/enum';

interface IProps {
  data: IRole[];
  onRowClick: (role: IRole) => void;
}

const TableRole = ({
  data,
  onRowClick
}: IProps) => {

  const [activeRow, setActiveRow] = useState<IRole | null>(null);

  // handle row click
  const handleRowClick = (role: IRole) => {
    setActiveRow(role);
    onRowClick(role);
  };

  const renderRowEmpty = () => {
    return (
      <TableRowEmpty message="No data available." />
    );
  };

  const renderRoleRow = () => {
    return (
      data.map((role) => (
        <TableRoleRow
          key={role.id}
          data={role}
          onRowClick={handleRowClick}
          isActive={role === activeRow}
        />
      ))
    );
  };

  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell size={SIZE.SMALL} type="th"></TableCell>
          <TableCell size={SIZE.LARGE} type="th">Name</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {data.length === 0 ? renderRowEmpty() : renderRoleRow()}
      </TableBody>
    </Table>
  );
};

export default memo(TableRole);
