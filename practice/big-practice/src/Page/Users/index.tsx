// library
import { useCallback, useMemo, useState } from 'react';

// types
import { IUser } from '@/types/users';

// components
import TableUser from '@/Page/Users/TableUser';
import UserSidebar from './UserSidebar';
import Appbar from '@/components/Appbar';

// constants
import { API_ERROR_MESSAGES } from '@/constants/message';

// hooks
import { useRoles, useRules, useUserStore, useUsers } from '@/hooks';

// css
import '@/Page/page.css'

// FontAwesome
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import SearchBar from '@/components/SearchBar';

// helper
import { filterData } from '@/helpers/search';

const UserManager = () => {
  const [showSidebarUser, setShowSidebarUser] = useState<boolean>(false);
  const [selectedUser, setSelectedUser] = useState<IUser | null>(null);
  const [hidden, setHidden] = useState(false);
  const [searchValue, setSearchValue] = useState('');
  const { stateUser } = useUserStore();

  //Get data list
  const { data: rolesData, error: rolesError } = useRoles();
  const { data: usersData, error: usersError } = useUsers();
  const { data: rulesData, error: rulesError } = useRules();

  if (rolesError || usersError || rulesError) {
    alert(API_ERROR_MESSAGES.GET_API);
  }

  // Handle click row data
  const handleUserSelected = (user: IUser) => {
    setSelectedUser(user);
    setShowSidebarUser(true);
  };

  const setStateSidebar = useCallback(setShowSidebarUser, [setShowSidebarUser]);

  // close
  const onCloseSearchBar = () => {
    setHidden(false)
    setSearchValue('');
  }

  const handleSearchUser = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();

    const { value } = event.target;
    setSearchValue(value);
  };

  // Search filter user
  const filterUser = useMemo(() => {
    return filterData<IUser>(usersData?.list, searchValue);
  }, [usersData, searchValue]);

  // Change AppBar
  const onChangeAppBar = () => {
    setHidden(!hidden)
  }

  return (
    <div className="d-flex">
      <div className="page-table">
        <div hidden={hidden}>
          <Appbar
            title="User information"
            icon={faSearch}
            isIcon
            itemClick={onChangeAppBar}
          />
        </div>

        <div hidden={!hidden}>
          <SearchBar
            inputValue={searchValue}
            onClose={onCloseSearchBar}
            onSearch={handleSearchUser}
          />
        </div>
        <TableUser data={filterUser} onRowClick={handleUserSelected} />
      </div>

      {showSidebarUser && selectedUser && (
        <UserSidebar
          item={selectedUser}
          rules={rulesData?.list}
          roles={rolesData?.list}
          stateSidebar={setStateSidebar} />
      )}
    </div>
  )
};

export default UserManager
