// components
import Table from '@/components/Table';
import TableCell from '@/components/Table/Cell';
import TableHead from '@/components/Table/Head';
import TableRow from '@/components/Table/Row'
import TableRowEmpty from '@/components/Table/Row/Empty';
import TableBody from '@/components/Table/Body';
import TableUserRow from '@/Page/Users/TableUserRow';

// Types
import { IUser } from '@/types/users'

// constants
import { SIZE } from '@/constants/enum';
import { useState } from 'react';

interface IProps {
  data: IUser[];
  onRowClick: (user: IUser) => void;
}

const TableUser = ({
  data,
  onRowClick
}: IProps) => {

  const [activeRow, setActiveRow] = useState<IUser | null>(null);

  // handle row click
  const handleRowClick = (user: IUser) => {
    setActiveRow(user);
    onRowClick(user);
  };

  const renderRowEmpty = () => {
    return (
      <TableRowEmpty message="No data available." />
    );
  };

  const renderUserRow = () => {
    return (
      data.map((user) => (
        <TableUserRow
          key={user.id}
          data={user}
          onRowClick={handleRowClick}
          isActive={user === activeRow}
        />
      ))
    );
  };

  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell size={SIZE.SMALL} type='th'></TableCell>
          <TableCell size={SIZE.LARGE} type='th'>Full Name</TableCell>
          <TableCell size={SIZE.MEDIUM} type='th'>Status</TableCell>
          <TableCell type='th'>Email</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {data.length === 0 ? renderRowEmpty() : renderUserRow()}
      </TableBody>
    </Table>
  );
};

export default TableUser;
