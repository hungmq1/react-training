
import { ComponentMeta, ComponentStory } from '@storybook/react';

// components
import TableUser from './TableUser';

export default {
  title: 'TableUser',
  component: TableUser,
} as ComponentMeta<typeof TableUser>;

const Template: ComponentStory<typeof TableUser> = (args) => <TableUser {...args} />;

export const HasData = Template.bind({});
HasData.args = {
  data: [
    {
      id: 1,
      name: 'John Doe',
      status: true,
      email: 'john.doe@example.com',
      avatar: '',
      registered: new Date,
      lastUpdate: new Date
    },
    {
      id: 2,
      name: 'Inculi Nano',
      status: false,
      email: '',
      avatar: '',
      registered: new Date,
      lastUpdate: new Date
    },
  ],
  onRowClick: (user) => {
    alert(user.name);
  },
};

export const NoData = Template.bind({});
NoData.args = {
  data: [],
  onRowClick: (user) => {
    console.log(user);
  },
};
