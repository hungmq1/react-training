// library
import { memo, useEffect, useState, useMemo, Dispatch, useCallback } from 'react'
import moment from 'moment';
import { mutate } from 'swr';

// layouts
import FormView from '@/layouts/View'
import EditInfo from '@/layouts/Edit/EditInfo';
import EditRules from '@/layouts/Edit/Rules';
import EditCheckBox from '@/layouts/Edit/CheckBox';

//types
import { IUser } from '@/types/users';
import { IRule } from '@/types/rules';
import { IRole } from '@/types/roles';

// constants
import { FORMAT, SIZE } from '@/constants/enum';
import { API_ERROR_MESSAGES, INPUT_ERROR_MESSAGES } from '@/constants/message';
import { listTabUsers } from '@/constants/listItem';

// helper
import { convertBase64 } from '@/helpers/converts';
import { validateEmail, validateEmpty } from '@/helpers/validate';
import { findById, findListId } from '@/helpers/find';
import { formatItem } from '@/helpers/format';

// Font Awesome
import { faArrowLeft, faListCheck, faPen, faUserShield } from '@fortawesome/free-solid-svg-icons';

// css
import '@/Page/page.css'

// service
import { removeUser, updateUser } from '@/services/user';

// components
import Appbar from '@/components/Appbar';
import IconButton from '@/components/Button/IconButton';
import Tabs from '@/components/Tabs';
import TabPanel from '@/components/Tabs/Panel'
import Input from '@/components/Input';
import InputFile from '@/components/Input/File';
import Avatar from '@/components/Avatar';
import SwitchButton from '@/components/Button/SwitchButton';
import Status from '@/components/Status';
import Modal from '@/components/Modal';

// hooks
import { useUserStore } from '@/hooks';

interface IProps {
  item: IUser;
  rules: IRule[];
  roles: IRole[];
  stateSidebar: Dispatch<React.SetStateAction<boolean>>;
}

const UserSidebar = ({
  item,
  rules,
  roles,
  stateSidebar
}: IProps) => {

  const [hidden, setHidden] = useState(false);
  const [value, setValue] = useState<string>('1');
  const [openModal, setOpenModal] = useState(false);
  const [data, setData] = useState<IUser>(item);
  const [baseImage, setBaseImage] = useState('');
  const { handleUpdateUser, handleDeleteUser } = useUserStore();

  // Format
  const emailFormat = useMemo(() => data.email || FORMAT.UNKNOWN, [data.email]);
  const registeredFormat = useMemo(() => moment(data.registered).format(FORMAT.DATE), [data.registered]);
  const lastVisitFormat = useMemo(() => moment(data.lastUpdate).format(FORMAT.DATE), [data.lastUpdate]);
  const isActive = data.status;

  useEffect(() => {
    setData(item);
  }, [item]);

  // handle change tab
  const handleChange = useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
    setValue(event.currentTarget.value);
  }, []);

  // hande Update User
  const handleSave = useCallback(async () => {

    // Validate Input Empty
    const errorMessage = validateEmpty(data.name, INPUT_ERROR_MESSAGES.INPUT_EMPTY);

    if (errorMessage) {
      alert(errorMessage);
      return;
    }

    // Validate the email
    if (!validateEmail(data.email)) {
      alert(INPUT_ERROR_MESSAGES.EMAIL_INVALID);
      return;
    }

    const update = {
      ...data,
      lastUpdate: new Date(),
      avatar: data.avatar || baseImage
    };

    const usersUpdate = await updateUser(data.id, update);

    handleUpdateUser(data);

    // update the user data with mutate
    mutate('/users');

    if (usersUpdate.error) {
      alert(API_ERROR_MESSAGES.UPDATE)
      return;
    }
  }, []);

  // hande Open Modal Delete
  const handleOpenModal = useCallback(() => {
    setOpenModal(true);
  }, []);

  // handle Close Modal Delete
  const handeCloseModal = useCallback(() => {
    setOpenModal(false);
  }, []);

  // handle Remove User
  const handleRemoveUser = useCallback(async () => {
    const remove = await removeUser(data.id);

    handleDeleteUser(data);

    // update the user data with mutate
    mutate('/users');

    if (remove.error) {
      alert(API_ERROR_MESSAGES.DELETE);
      return;
    }

    stateSidebar(false);
    setOpenModal(false);
  }, []);

  // Handle change input
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setData(prev => ({
      ...prev,
      [name]: value
    }));
  }

  // Handle Change avatar
  const handleFileChange = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const files = event.target.files
    const file = files && files[0];
    const base64 = await convertBase64(file!) as string;

    setBaseImage(base64)
  };

  // Handle change status
  const handleChangeStatus = () => {
    setData(prev => ({
      ...prev,
      status: !prev.status
    }));
  }

  // Change AppBar
  const onChangeAppBar = () => {
    setHidden(!hidden)
  };

  // close TabBar
  const onCloseTabBar = () => {
    setHidden(false)
  };

  // handle  Checkbox rules
  const handleCheckRule = async (id: string, checked: boolean) => {
    const convertId = Number(id);
    const ruleActive = item.rules;
    const listRulesAssigned = item.rulesAssigned;
    const listRulesRoles = item.rulesRole || {};

    if (ruleActive && listRulesAssigned) {

      if (checked) {
        ruleActive.push(convertId);
        ruleActive.sort();

        if (!listRulesAssigned.includes(convertId)) {
          listRulesAssigned.push(convertId);
          listRulesAssigned.sort();
        }

        // Check if the rule has been assigned to the user according to any role
        listRulesRoles[id] = [
          ...(listRulesRoles[id] || []),
        ]

      } else {
        ruleActive.splice(ruleActive.indexOf(convertId), 1);
        listRulesAssigned.splice(listRulesAssigned.indexOf(convertId), 1);

        if (listRulesRoles[id]) {
          delete listRulesRoles[id]
        }
      }
    }

    const updateRules = {
      ...item,
      rules: ruleActive,
      rulesAssigned: listRulesAssigned,
      rulesRole: listRulesRoles,
    };

    const editRulesUser = await updateUser(updateRules.id, updateRules);

    handleUpdateUser(editRulesUser.list);
  };

  // handle checkbox roles
  const handleCheckRole = async (id: string, checked: boolean) => {
    const convertId = Number(id);
    const roleActive = item.roles;
    const listRulesAssigned = item.rulesAssigned;
    const listRulesRoles = Object.entries(item.rulesRole!);
    const listIdRules = findListId('role', 'RoleRule', convertId, rules);

    if (roleActive && listRulesRoles) {

      if (checked) {
        roleActive.push(convertId);
        roleActive.sort();

        listIdRules.forEach((idRule) => {
          // Find item includes the rule
          const ruleRolesFind = listRulesRoles.find((item) => item[0] === idRule.toString());

          // if not found rule
          if (ruleRolesFind === undefined) {
            listRulesRoles.push([idRule.toString(), [convertId]]);
          } else {
            ruleRolesFind[1].push(convertId);
            ruleRolesFind[1].sort();
          };

          // Update listRulesAssigned
          if (listRulesAssigned!.includes(idRule) === false) {
            listRulesAssigned!.push(idRule);
            listRulesAssigned!.sort();
          }
        });
      } else {
        roleActive.splice(roleActive.indexOf(convertId), 1);

        listRulesRoles.filter((data) => {

          if (
            data[1].length === 1 &&
            data[1].includes(convertId) &&
            !item.rules?.includes(Number(data[0]))
          ) {
            listRulesAssigned!.filter((number) => number !== Number(data[0]));
            return false;
          }

          // If list id role includes role, remove it and return true
          if (data[1].includes(convertId)) {
            data[1] = data[1].filter((number) => number !== convertId);
            return true;
          }

          return true;
        });
      }
    }

    const updateRoles = {
      ...item,
      roles: roleActive,
      rulesAssigned: listRulesAssigned,
      rulesRole: Object.fromEntries(listRulesRoles),
    };

    const editRolesUser = await updateUser(updateRoles.id, updateRoles);

    handleUpdateUser(editRolesUser.list);
  }

  // set Rules active in users
  const rulesActive = item.rules!.map((rule) => findById<IRule>(rules, rule)!);

  // set roles active in users
  const rolesActive = item.roles!.map((role) => findById<IRole>(roles, role)!);

  // set rules Assigned
  const rulesAssigned = item.rules! ? item.rules.map((rule) => findById<IRule>(rules, rule)!) : null;

  // list roles active form view
  const listRolesActive = item.roles
    ? formatItem(
      item.roles.map((role) => findById<IRole>(roles, role)!),
      'id',
      'name',
    ) : [];

  // list rules Assigned form view
  const listRulesAssigned = item.rules
    ? formatItem(
      item.rules.map((rule) => findById<IRule>(rules, rule)!),
      'id',
      'description',
    ) : [];

  // render Form view
  const renderFormView = ({
    id,
    avatar,
    name
  }: IUser) => {

    return (
      <>
        <FormView
          hasAvatar
          avatar={avatar}
          name={name}
          email={{
            title: 'Email',
            listItem: [{ id: id.toString(), value: emailFormat }]
          }}
          lastVisited={{
            title: 'lastVisited',
            listItem: [{ id: id.toString(), value: lastVisitFormat }]
          }}
          roles={{
            title: 'Roles assigned',
            icon: faUserShield,
            listItem: listRolesActive.map(item => ({ id: item.id!, value: item.value })),
            count: item.roles?.length
          }}
          rules={{
            title: 'Rules assigned',
            icon: faListCheck,
            listItem: listRulesAssigned.map(item => ({ id: item.id!, value: item.value })),
            count: item.rules?.length
          }}
        />
      </>
    )
  };

  // render Tab edit General
  const renderTabPanel = () => {
    return (
      <>
        <TabPanel index={value} value={'1'}>
          <EditInfo
            btnTextPrimary='Save'
            btnTextSecondary='Delete'
            onClickPrimary={handleSave}
            onClickSecondary={handleOpenModal}>
            <div className="spacing-normal">
              <Input
                size={SIZE.SMALL}
                label='Full Name'
                name='name'
                value={data.name}
                onChange={handleInputChange} />
            </div>
            <div className="spacing-normal">
              <Input
                size={SIZE.SMALL}
                label='Email'
                name='email'
                value={data.email}
                onChange={handleInputChange} />
            </div>
            <div className="spacing-normal page-edit-block d-flex">
              <InputFile
                label='Avatar'
                labelText='Upload New Photo'
                name='fileAvatar'
                handleChange={handleFileChange}
                htmlFor='avatar'
              />
              <Avatar
                name={data.name}
                avatar={data.avatar || baseImage}
                size={SIZE.MEDIUM} />
            </div>
            <div className="spacing-normal page-edit-block d-flex">
              <p className="page-edit-title">Status:</p>
              <SwitchButton isChecked={isActive} onChange={handleChangeStatus} />
              <Status isActive={isActive} />
            </div>
            <div className="spacing-top-medium">
              <div className='page-edit-block d-flex spacing-normal'>
                <p className="page-edit-title">Registered:</p>
                <p>{registeredFormat}</p>
              </div>
              <div className='page-edit-block d-flex spacing-normal spacing-top-normal'>
                <p className="page-edit-title">Last visited:</p>
                <p>{lastVisitFormat}</p>
              </div>
            </div>
          </EditInfo>
        </TabPanel>

        <TabPanel index={value} value={'2'}>
          <EditRules
            isChecked
            name={item.name}
            rules={rules}
            rulesActive={rulesActive}
            roles={roles!}
            rulesAssigned={rulesAssigned}
            rulesRole={item.rulesRole}
            onChange={handleCheckRule}
          />
        </TabPanel>

        <TabPanel index={value} value={'3'}>
          <EditCheckBox
            name={item.name}
            list={roles}
            listActive={rolesActive}
            type='roles'
            onChange={handleCheckRole}
          />
        </TabPanel>
      </>
    )
  };

  return (
    <>
      <div hidden={hidden} className="page-sidebar page-view">
        <Appbar
          title="User information"
          icon={faPen}
          isIcon
          isStatus
          isActive={data.status}
          itemClick={onChangeAppBar}
        />
        {renderFormView(item)}
      </div>
      <div hidden={!hidden} className="page-sidebar page-edit">
        <div className="d-flex page-edit-header">
          <IconButton
            icon={faArrowLeft}
            onClick={onCloseTabBar}
          />
          <Tabs
            list={listTabUsers}
            index={value}
            onClick={handleChange}
          />
        </div>
        {renderTabPanel()}
      </div>

      {openModal ? (
        <Modal
          isOpen={openModal}
          title="Are you sure to delete this user?"
          onCloseModal={handeCloseModal}
          btnNamePrimary='Delete'
          btnNameSecondary='Cancel'
          onClickBtnPrimary={handleRemoveUser}
          onClickBtnSecondary={handeCloseModal}>
        </Modal>
      ) : null}
    </>
  )
};

export default memo(UserSidebar);
