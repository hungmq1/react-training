// components
import Avatar from '@/components/Avatar';
import Label from '@/components/Status';
import TableRow from '@/components/Table/Row';
import TableCell from '@/components/Table/Cell';

// Types
import { IUser } from '@/types/users'

// constant
import { SIZE } from '@/constants/enum'

interface IProps {
  data: IUser;
  isActive?: boolean;
  onRowClick: (user: IUser) => void;
}

const TableUserRow = ({
  data,
  isActive = false,
  onRowClick
}: IProps) => {

  // handle Row Click
  const handleRowClick = () => {
    if (onRowClick) {
      onRowClick(data);
    }
  };

  return (
    <TableRow
      isActive={isActive}
      onRowClick={handleRowClick}
    >
      <TableCell size={SIZE.SMALL} type='td'>
        <Avatar
          name={data.name}
          avatar={data.avatar}
          circle={true}
          size={SIZE.SMALL} />
      </TableCell>
      <TableCell size={SIZE.LARGE} type='td'>{data.name}</TableCell>
      <TableCell size={SIZE.MEDIUM} type='td'>
        <Label isActive={data.status} />
      </TableCell>
      <TableCell type='td'>{data.email}</TableCell>
    </TableRow>
  )
};

export default TableUserRow;
