import { useState } from 'react';

// components
import Table from '@/components/Table';
import TableCell from '@/components/Table/Cell';
import TableHead from '@/components/Table/Head';
import TableRow from '@/components/Table/Row'
import TableRowEmpty from '@/components/Table/Row/Empty';
import TableBody from '@/components/Table/Body';
import TableRuleRow from '@/Page/Rules/TableRuleRow';

// Types
import { IRule } from '@/types/rules'

// constants
import { SIZE } from '@/constants/enum';

interface IProps {
  data: IRule[];
  onRowClick: (rule: IRule) => void;
}

const TableRule = ({ data, onRowClick }: IProps) => {

  const [activeRow, setActiveRow] = useState<IRule | null>(null);

  // handle row click
  const handleRowClick = (rule: IRule) => {
    setActiveRow(rule);
    onRowClick(rule);
  };

  const renderRowEmpty = () => {
    return (
      <TableRowEmpty message="No data available." />
    );
  };

  const renderRuleRow = () => {
    return (
      data.map((rule) => (
        <TableRuleRow
          key={rule.id}
          data={rule}
          onRowClick={handleRowClick}
          isActive={rule === activeRow}
        />
      ))
    );
  };

  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell size={SIZE.LARGE} type='th'>Name</TableCell>
          <TableCell size={SIZE.LARGE} type='th'>Description</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {data.length === 0 ? renderRowEmpty() : renderRuleRow()}
      </TableBody>
    </Table>
  );
};

export default TableRule;
