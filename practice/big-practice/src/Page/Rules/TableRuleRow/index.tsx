// components
import TableRow from '@/components/Table/Row';
import TableCell from '@/components/Table/Cell';

// Types
import { IRule } from '@/types/rules'

// constant
import { SIZE } from '@/constants/enum'

interface IProps {
  data: IRule;
  isActive?: boolean;
  onRowClick: (role: IRule) => void;
}

const TableRuleRow = ({
  data,
  isActive = false,
  onRowClick
}: IProps) => {

  // handle Row Click
  const handleRowClick = () => {

    if (onRowClick) {
      onRowClick(data);
    }
  };

  return (
    <TableRow
      isActive={isActive}
      onRowClick={handleRowClick}
    >
      <TableCell size={SIZE.LARGE} type='td'>
        {data.name}
      </TableCell>
      <TableCell size={SIZE.LARGE} type='td'>{data.description}</TableCell>
    </TableRow>
  )
};

export default TableRuleRow;
