
import { ComponentMeta, ComponentStory } from '@storybook/react';

// components
import TableRule from './TableRule';

export default {
  title: 'TableRule',
  component: TableRule,
} as ComponentMeta<typeof TableRule>;

const Template: ComponentStory<typeof TableRule> = (args) => <TableRule {...args} />;

export const HasData = Template.bind({});
HasData.args = {
  data: [
    {
      id: 1,
      name: 'CanSeeUsers',
      description: 'Can see user details and access levels'
    },
    {
      id: 2,
      name: 'CanEditUsers',
      description: 'Can modify user details and access levels'
    },
  ],
  onRowClick: (role) => {
    alert(role.name);
  },
};

export const NoData = Template.bind({});
NoData.args = {
  data: [],
  onRowClick: (user) => {
    console.log(user);
  },
};
