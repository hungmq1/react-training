import useSWR from 'swr';
import { useMemo, useState } from 'react';

// components
import TableRule from './TableRule';
import RuleSidebar from './RuleSidebar';
import Appbar from '@/components/Appbar';
import SearchBar from '@/components/SearchBar';

// helpers
import { filterData } from '@/helpers/search';

// constants
import { API_ERROR_MESSAGES } from '@/constants/message';

// types
import { IRule } from '@/types/rules';

// css
import '@/Page/page.css';

// FontAwesome
import { faSearch } from '@fortawesome/free-solid-svg-icons';

// hooks
import { useRules } from '@/hooks';

const RuleManager = () => {
  const [showSidebarRule, setShowSidebarRule] = useState<boolean>(false);
  const [selectedRule, setSelectedRule] = useState<IRule | null>(null);
  const [hidden, setHidden] = useState(false);
  const [searchValue, setSearchValue] = useState('');

  const { data, error } = useRules();

  if (error) {
    alert(API_ERROR_MESSAGES.GET_API);
  };

  // Handle click row data
  const handleRuleSelected = (rule: IRule) => {
    setSelectedRule(rule)
    setShowSidebarRule(true);
  };

  // Search filter rules
  const filterRule = useMemo(() => {
    return filterData<IRule>(data?.list, searchValue);
  }, [data, searchValue]);

  // close searchBar
  const onCloseSearchBar = () => {
    setHidden(false)
    setSearchValue('');
  }

  // Change AppBar
  const onChangeAppBar = () => {
    setHidden(!hidden)
  }

  // change input search
  const handleSearchRule = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();

    const { value } = event.target;
    setSearchValue(value);
  };

  return (
    <div className="d-flex">
      <div className="page-table">
        <div hidden={hidden}>
          <Appbar
            title="Rule information"
            icon={faSearch}
            isIcon
            itemClick={onChangeAppBar}
          />
        </div>
        <div hidden={!hidden}>
          <SearchBar
            inputValue={searchValue}
            onClose={onCloseSearchBar}
            onSearch={handleSearchRule}
          />
        </div>
        <TableRule data={filterRule} onRowClick={handleRuleSelected} />
      </div>
      {showSidebarRule && selectedRule && (
        <RuleSidebar
          item={selectedRule} />
      )}
    </div>
  )
};

export default RuleManager;
