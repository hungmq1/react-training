// library
import { memo } from 'react'

// layouts
import FormView from '@/layouts/View'

//types
import { IRule } from '@/types/rules';

// css
import '@/Page/page.css'

// components
import Appbar from '@/components/Appbar';

interface IProps {
  item: IRule;
}

const RuleSidebar = ({
  item,
}: IProps) => {

  // render Form view
  const renderFormView = ({
    name,
    description
  }: IRule) => {

    return (
      <FormView
        name={name}
        description={description}
      />
    )
  };

  return (
    <>
      <div className="page-sidebar page-view">
        <Appbar
          title="Rule information" />
        {renderFormView(item)}
      </div>
    </>
  )
};

export default memo(RuleSidebar);
