enum SIZE {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
}

enum TYPE {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  MODIFY = 'Modify',
  DONE = 'Done',
}

enum STATUS {
  ACTIVE = 'Active',
  NOT_ACTIVE = 'Not active'
}

enum FORMAT {
  DATE = 'MMM DD, YYYY hh:mm:ss',
  UNKNOWN = 'Unknown'
  }

export { SIZE, TYPE, STATUS, FORMAT }
