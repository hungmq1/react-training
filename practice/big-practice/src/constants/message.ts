// Message API
const API_ERROR_MESSAGES = {
  GET_API: 'Failed to retrieve data',
  ADD: 'Failed to add new data',
  UPDATE: 'Failed to update data',
  DELETE: 'Failed to delete data',
};

// Message input error
const INPUT_ERROR_MESSAGES = {
  EMAIL_INVALID: 'Invalid email address',
  INPUT_EMPTY: 'Input cannot be empty',
};

export {
  API_ERROR_MESSAGES,
  INPUT_ERROR_MESSAGES
}
