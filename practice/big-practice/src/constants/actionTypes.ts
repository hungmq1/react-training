const ACTION = {
  ADD: 'ADD',
  SELECTED: 'SELECTED',
  UPDATE: 'UPDATE',
  DELETE: 'DELETE'
};

export { ACTION }
