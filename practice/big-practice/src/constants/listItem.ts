import { ITab } from "@/types/tab";
import { faListCheck, faUserGroup, faUserShield } from "@fortawesome/free-solid-svg-icons";

// list item page
const listItem = [
  {
    name: 'Users',
    icon: faUserGroup,
    path: '/'
  },
  {
    name: 'Roles',
    icon: faUserShield,
    path: '/role'
  },
  {
    name: 'Rules',
    icon: faListCheck,
    path: '/rule'
  }
];

// list Tab Page Users
const listTabUsers: Omit<ITab, 'index' | 'onClick'>[] = [
  {
    title: 'General',
    value: '1',
  },
  {
    title: 'Rules',
    value: '2',
  },
  {
    title: 'Roles',
    value: '3',
  },
];


  // List Tab Page roles
  const listTabRoles: Omit<ITab, 'index' | 'onClick'>[] = [
    {
      title: 'General',
      value: '1',
    },
    {
      title: 'Rules',
      value: '2',
    },
    {
      title: 'Members',
      value: '3',
    },
  ];
export { listItem, listTabUsers, listTabRoles }
