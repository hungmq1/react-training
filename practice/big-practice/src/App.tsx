// css
import './App.css'

// library
import { Route, Routes } from 'react-router-dom';

// components
import LayoutDefault from '@/layouts/Default';

// pages
import UserManager from '@/Page/Users';
import RoleManager from '@/Page/Roles';
import RuleManager from './Page/Rules';

function App() {

  return (
    <>
      <LayoutDefault>
        <Routes>
          <Route path="/" element={<UserManager />} />
          <Route path="/role" element={<RoleManager />} />
          <Route path="/rule" element={<RuleManager />} />
        </Routes>
      </LayoutDefault>

    </>
  );
};

export default App
