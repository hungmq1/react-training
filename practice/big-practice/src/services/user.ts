// library
import axios from 'axios';

// constants
import { URL_API } from '@/constants/api';
import { IUser } from '@/types/users';

axios.defaults.baseURL = URL_API;

/**
 * Get users from json-server
 * @returns {Object}
 */
const getUsers = async () => {
  try {
    const response = await axios.get(`/users`);

    return {
      list: response.data,
      error: null,
    };
  } catch (error) {

    return {
      list: null,
      error,
    };
  }
};

/**
 * @param {IUser} payload
 * @returns {Object}
 */
const createUser = async (payload: Omit<IUser, 'id'>) => {
  try {
    const response = await axios.post('/users', payload);

    return {
      list: response.data,
      error: null,
    };
  } catch (error) {

    return {
      list: null,
      error,
    };
  }
};

/**
 * Update user
 * @param {Number} id
 * @param {IUser} payload
 * @returns {Object}
 */
const updateUser = async (id: number, payload: IUser) => {
  try {
    const response = await axios.patch(`/users/${id}`, payload);

    return {
      list: response.data,
      error: null,
    };
  } catch (error) {

    return {
      list: null,
      error,
    };
  }
};

/**
 * Remove user by id
 * @param {Number} id - id of user
 *
 * @returns {Object}
 */
const removeUser = async (id: number) => {
  try {
    const response = await axios.delete(`/users/${id}`);

    return {
      list: response.data,
      error: null,
    };
  } catch (error) {

    return {
      list: null,
      error,
    };
  }
};

export { getUsers, createUser, updateUser, removeUser }
