// library
import axios from 'axios';

// constants
import { URL_API } from '@/constants/api';

// types
import { IRole } from '@/types/roles';

axios.defaults.baseURL = URL_API;

/**
 * Get Roles from json-server
 * @returns {Object}
 */
const getRoles = async () => {
  try {
    const response = await axios.get(`/roles`);

    return {
      list: response.data,
      error: null,
    };
  } catch (error) {

    return {
      list: null,
      error,
    };
  }
};

/**
 * @param {IRole} payload
 * @returns {Object}
 */
const createRole = async (payload: Omit<IRole, 'id'>) => {
  try {
    const response = await axios.post('/roles', payload);

    return {
      list: response.data,
      error: null,
    };
  } catch (error) {

    return {
      list: null,
      error,
    };
  }
};

/**
 * Update Role
 * @param {Number} id
 * @param {IRole} payload
 * @returns {Object}
 */
const updateRole = async (id: number, payload: IRole) => {
  try {
    const response = await axios.patch(`/roles/${id}`, payload);

    return {
      list: response.data,
      error: null,
    };
  } catch (error) {

    return {
      list: null,
      error,
    };
  }
};

/**
 * Remove role by id
 * @param {Number} id - id of role
 *
 * @returns {Object}
 */
const removeRole = async (id: number) => {
  try {
    const response = await axios.delete(`/roles/${id}`);

    return {
      list: response.data,
      error: null,
    };
  } catch (error) {

    return {
      list: null,
      error,
    };
  }
};

export { getRoles, createRole, updateRole, removeRole }
