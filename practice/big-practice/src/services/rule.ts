// library
import axios from 'axios';

// constants
import { URL_API } from '@/constants/api';

axios.defaults.baseURL = URL_API;

/**
 * Get users from json-server
 * @returns {Object}
 */
const getRules = async () => {
  try {
    const response = await axios.get(`/rules`);

    return {
      list: response.data,
      error: null,
    };
  } catch (error) {

    return {
      list: null,
      error,
    };
  }
};

export { getRules }
