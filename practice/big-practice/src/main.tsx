import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import App from './App'
import UserProvider from '@/contexts/user/Provider'
import RoleProvider from './contexts/role/Provider'
import './index.css'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <BrowserRouter>
      <UserProvider>
        <RoleProvider>
        <App />
        </RoleProvider>
      </UserProvider>
    </BrowserRouter>
  </React.StrictMode>,
)
