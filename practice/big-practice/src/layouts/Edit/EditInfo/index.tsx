// library
import { memo, ReactNode } from "react";

// components
import Button from "@/components/Button";

// constants
import { SIZE, TYPE } from "@/constants/enum";

// css
import './editInfo.css'

interface IProps {
  children: ReactNode;
  btnTextPrimary: string;
  onClickPrimary: () => void;
  btnTextSecondary: string;
  onClickSecondary: () => void;
}

const EditInfo = ({
  children,
  btnTextPrimary,
  onClickPrimary,
  btnTextSecondary,
  onClickSecondary,
}: IProps) => {

  return (
    <div className="edit">
      <div className="edit-wrapper d-flex">
        <Button
          size={SIZE.SMALL}
          label={btnTextSecondary}
          onClick={onClickSecondary} />
        <Button
          size={SIZE.SMALL}
          label={btnTextPrimary}
          typeButton={TYPE.PRIMARY}
          onClick={onClickPrimary}
        />
      </div>
      <div className="edit-item">{children}</div>
    </div>
  )
};

export default memo(EditInfo)
