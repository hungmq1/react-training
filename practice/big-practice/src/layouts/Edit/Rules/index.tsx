import { memo, useMemo, useState } from 'react'
import clsx from 'clsx';

// types
import { IRole } from '@/types/roles';
import { IRule } from '@/types/rules';
import { IRulesRoles } from '@/types/rolesRules';

// css
import './editRules.css'

// helper
import { filterData } from '@/helpers/search';

// components
import RadioButton from '@/components/RadioButton';
import Button from '@/components/Button';
import CheckBox from '@/components/CheckBox';
import Label from '@/components/Label';

// constants
import { SIZE, TYPE } from '@/constants/enum';
import { resultRulesRoles } from '@/helpers/transform';

interface IProps {
  name: string;
  rules: IRule[];
  rulesActive: IRule[];
  isChecked?: boolean;
  roles?: IRole[];
  onChange: (id: string, checked: boolean) => void;
  rulesRole?: { [key: string]: number[] };
  rulesAssigned?: IRule[] | null;
}

const EditRules = ({
  name,
  rules,
  rulesActive,
  isChecked,
  onChange,
  rulesRole,
  roles,
  rulesAssigned
}: IProps) => {
  const [isDirectRules, setIsDirectRules] = useState(true);
  const [isDisabled, setIsDisabled] = useState(false);
  const [btnName, setBtnName] = useState(TYPE.MODIFY);
  const [isOpened, setIsOpened] = useState(false);
  const [searchValue, setSearchValue] = useState('');

  // Search filter all
  const searchResult = useMemo(() => {
    return filterData<IRule>(rules, searchValue);
  }, [rules, searchValue]);

  // Search filter rule directly
  const searchRuleDirected = useMemo(() => {
    return filterData<IRule>(rulesActive, searchValue);
  }, [rulesActive, searchValue]);

  // Open list Assigned directly
  const onCheckDirectly = () => {
    setIsDirectRules(true);
    setIsDisabled(false);
  };

  // Open All assignments
  const onCheckAssignments = () => {
    setIsDisabled(true);
    setIsDirectRules(false);
    setBtnName(TYPE.MODIFY);
    setIsOpened(false);
  };

  // Toggle edit mode
  const toggleButton = () => {
    setIsOpened(prev => !prev);
    setBtnName(prev => prev === TYPE.MODIFY ? TYPE.DONE : TYPE.MODIFY);
    setSearchValue('')
  };

  // change input search
  const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    setSearchValue(value);
  };

  // Renders the Edit Rule component
  const renderEditRule = () => {
    return searchResult.map((rule) => {
      const ruleChecked = rulesActive.includes(rule);
      return (
        <div
          key={rule.id}
          className={clsx('edit-rules-item', { 'edit-rules-item--checked': ruleChecked })}
        >
          <CheckBox
            id={rule.id.toString()}
            checked={ruleChecked}
            onChange={onChange}>
            <p className='edit-rules-item-title'>{rule.name}</p>
            <p className="edit-rules-item-des">{rule.description}</p>
          </CheckBox>
        </div>
      )
    })
  };

  // Renders list Rule Active (Assigned directly)
  const renderAssignedDirectly = () => {
    return searchRuleDirected.map((rule) => {
      if (rule) {
        return (
          <div
            key={rule.id}
            className='edit-rules-item edit-rules-item--checked'
          >
            <p className='edit-rules-item-title'>{rule.name}</p>
            <p className="edit-rules-item-des">{rule.description}</p>
          </div>
        );
      }
    });
  };

  // result handling Role Rules
  const resultData = resultRulesRoles(rulesRole!, rules, rulesActive, roles!);

  // Search filter Rules roles
  const rulesRolesFilter = useMemo(() => {
    return filterData<IRulesRoles>(resultData, searchValue);
  }, [resultData, searchValue]);

  // Renders All assignments (Assigned directly & By roles)
  const renderAllAssignments = () => {

    return rulesRolesFilter.map((rule) => (
      <div key={rule.id} className='edit-rules-item'>
        <div className="d-flex edit-rules-spacing edit-rules-item-block">
          <p className='edit-rules-item-title'>{rule.name}:</p>
          <p>{rule.description}</p>
        </div>
        <div className="d-flex edit-rules-spacing">
          {rule.assignedDirectly &&
            <Label
              name='Assigned directly'
              disable={true} />}

          {/* map roles from IRole */}
          {rule.roles.map((role) => {
            if (role)
              return (
                <Label
                  key={role.id}
                  name={role.name}
                  color={role.color}
                  disable={false}
                  icon />
              );
          })}
        </div>
      </div>
    ))
  };

  return (
    <>
      <div className="edit-rules">
        <p className="edit-rules-label">
          {name}
        </p>
        <div className="edit-rules-item-block d-flex d-flex-space-between">
          {!isChecked ? (
            rulesActive.length === 0 ? (
              <p>No rules assigned</p>
            )
              : (
                <p>{`Rules assigned (${rulesActive.length})`}</p>
              ))
            : (
              <div>
                <RadioButton
                  label='Assigned directly'
                  id='directly'
                  isChecked={isDirectRules}
                  count={rulesActive ? rulesActive.length : 0}
                  onChange={onCheckDirectly} />

                <RadioButton
                  label='All assignments'
                  id='assignments'
                  isChecked={!isDirectRules}
                  count={rulesAssigned ? rulesAssigned.length : 0}
                  onChange={onCheckAssignments} />
              </div>
            )}
          <Button
            size={SIZE.MEDIUM}
            label={btnName}
            onClick={toggleButton}
            disabled={isDisabled} />
        </div>
      </div>
      <div className="edit-rules-search d-flex d-flex-center">
        <input
          className='edit-rules-input'
          value={searchValue}
          onChange={handleSearch}
          placeholder='Search' />
      </div>
      <div className="edit-rules-list scroll">
        {isOpened ? renderEditRule() : (isDirectRules ? renderAssignedDirectly() : renderAllAssignments())}
      </div>
    </>
  )
}

export default memo(EditRules)
