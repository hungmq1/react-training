import { memo, useMemo, useState } from 'react';
import clsx from 'clsx';

// css
import './editCheckbox.css';
import '@/layouts/Edit/Rules/editRules.css'

// constants
import { SIZE, TYPE } from '@/constants/enum';

// types
import { IRole } from '@/types/roles';
import { IUser } from '@/types/users';

// helpers
import { filterData } from '@/helpers/search';

// components
import Button from '@/components/Button';
import CheckBox from '@/components/CheckBox';
import Avatar from '@/components/Avatar';

interface IProps {
  name: string;
  list: Partial<IRole>[] | Partial<IUser>[];
  listActive: Partial<IRole>[] | Partial<IUser>[];
  onChange: (id: string, checked: boolean) => void;
  type: string;
}

const EditCheckBox = ({
  name,
  list,
  listActive,
  onChange,
  type
}: IProps) => {
  const [btnName, setBtnName] = useState(TYPE.MODIFY);
  const [isOpened, setIsOpened] = useState(false);
  const [searchValue, setSearchValue] = useState('');

  // Search filter all
  const searchResult = useMemo(() => {
    return filterData<IRole | IUser>(list as (IRole | IUser)[], searchValue);
  }, [list, searchValue]);

  // Search filter item active
  const searchItemActive = useMemo(() => {
    return filterData<IRole | IUser>(listActive as (IRole | IUser)[], searchValue);
  }, [listActive, searchValue]);

  // Toggle edit mode
  const toggleButton = () => {
    setIsOpened(prev => !prev);
    setBtnName(prev => prev === TYPE.MODIFY ? TYPE.DONE : TYPE.MODIFY);
  };

  // change input search
  const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    setSearchValue(value);
  };

  const renderItemActive = () => {
    return searchItemActive.map((item) => {
      let displayAttribute: string | undefined;

      if ("color" in item) {
        displayAttribute = item.color;
      } else {
        displayAttribute = item.avatar;
      }

      return (
        <div
          key={item.id}
          className='d-flex edit-checkbox-item edit-rules-item--checked'>
          <Avatar
            name={item.name!}
            size={SIZE.SMALL}
            circle
            color={displayAttribute}
            avatar={("avatar" in item) ? item.avatar : undefined}
          />
          <p className='edit-rules-item-title'>{item.name}</p>
        </div>
      );
    });
  };

  // render List User or Role
  const renderList = () => {
    return searchResult.map((item) => {
      let displayAttribute: string | undefined;

      if ("color" in item) {
        displayAttribute = item.color;
      } else {
        displayAttribute = item.avatar;
      }

      const checked = listActive.includes(item);

      return (
        <div
          key={item.id}
          className={clsx('edit-rules-item', { 'edit-rules-item--checked': checked })}
        >
          <CheckBox
            id={item.id!.toString()}
            checked={checked}
            onChange={onChange}
          >
            <div className='d-flex edit-rules-item-block'>
              <Avatar
                name={item.name}
                size={SIZE.SMALL}
                circle
                color={displayAttribute}
                avatar={("avatar" in item) ? item.avatar : undefined}
              />
              <p className='edit-rules-item-title'>{item.name}</p>
            </div>
          </CheckBox>
        </div>
      );
    });
  };

  return (
    <>
      <div className="edit-check">
        <p className="edit-check-label">
          {name}
        </p>
        <div className='edit-check-control d-flex d-flex-space-between'>
          {listActive!.length === 0 ? (
            <p>{`No ${type} assigned`}</p>
          ) : (
            <p>{`${type} assigined (${listActive!.length})`}</p>
          )}
          <Button
            size={SIZE.MEDIUM}
            label={btnName}
            onClick={toggleButton} />
        </div>
      </div>
      <div className="edit-rules-search d-flex d-flex-center">
        <input
          className='edit-rules-input'
          value={searchValue}
          onChange={handleSearch}
          placeholder='Search' />
      </div>
      <div className='edit-rules-list scroll'>
        {isOpened ? renderList() : renderItemActive()}
      </div>

    </>
  );
};

export default memo(EditCheckBox);
