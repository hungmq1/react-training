// library
import { ReactNode, useState } from 'react';

// css
import './layoutDefault.css'

// components
import Header from '@/components/Header';
import Sidebar from '@/components/Sidebar';
import Popover from '@/components/Popover';
import Modal from '@/components/Modal';
import Input from '@/components/Input';
import ListItem from '@/components/Sidebar/List';

// constants
import { SIZE } from '@/constants/enum';
import { listItem } from '@/constants/listItem';

// helpers
import { validateEmpty } from '@/helpers/validate';

import { INPUT_ERROR_MESSAGES, API_ERROR_MESSAGES } from '@/constants/message';

// service
import { createUser } from '@/services/user';
import { createRole } from '@/services/role';

// hooks
import { useRolesStore, useUserStore } from '@/hooks';
import { mutate } from 'swr';

interface IProps {
  children: ReactNode
}

const LayoutDefault = ({ children }: IProps) => {
  const [inputValue, setInputValue] = useState('')
  const [stateUserModal, setStateUserModal] = useState(false);
  const [stateRoleModal, setStateRoleModal] = useState(false);

  // hook
  const { handleAddUser } = useUserStore();
  const { handleAddRole } = useRolesStore();

  // list popover item
  const listPopoverItem = [
    {
      label: 'Add new user',
      onClick: () => setStateUserModal(true),
    },

    {
      label: 'Add new roles',
      onClick: () => setStateRoleModal(true),
    }
  ];

  // reset input
  const resetInput = () => {
    setInputValue('');
  }

  // Close user modal
  const handleCloseUserModal = () => {
    setStateUserModal(false);
    resetInput();
  };

  // Close user modal
  const handleCloseRoleModal = () => {
    setStateRoleModal(false);
    resetInput();
  };

  // handle Change
  const onChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.currentTarget.value)
  }

  // handle add new user
  const handleAddNewUser = async () => {
    const userName = inputValue;
    const errorMessage = validateEmpty(userName, INPUT_ERROR_MESSAGES.INPUT_EMPTY);

    if (errorMessage) {
      alert(errorMessage);
      return;
    }

    const payload = {
      name: userName,
      email: '',
      status: false,
      avatar: '',
      registered: new Date(),
      lastUpdate: new Date(),
      rules: [],
      roles: [],
      rulesAssigned: [],
      rulesRole: {}
    };

    const addUser = await createUser(payload);

    if (addUser.error) {
      alert(API_ERROR_MESSAGES.ADD);
      return;
    }

    handleCloseUserModal();

    // update the user data in the context
    handleAddUser(addUser.list);

    // update the user data with mutate
    mutate('/users');
  };

  // handle add new role
  const handleAddNewRole = async () => {
    const roleName = inputValue;
    const errorMessage = validateEmpty(roleName, INPUT_ERROR_MESSAGES.INPUT_EMPTY);

    if (errorMessage) {
      alert(errorMessage);
      return;
    }

    const roleAdd = {
      name: roleName,
      color: '',
      rules: [],
      users: [],
    };

    const addRole = await createRole(roleAdd);

    if (addRole.error) {
      alert(API_ERROR_MESSAGES.ADD);
      return;
    };

    handleAddRole(addRole.list);

    // update the roles data with mutate
    mutate('/roles');

    handleCloseRoleModal();
  }
  return (
    <>
      <Header>
        User Manager
      </Header>
      <div className="layout-wrapper">
        <div className="layout-row d-flex">
          <div>
            <Sidebar>
              <div className="layout-item">
                <Popover
                  labelText="New"
                  items={listPopoverItem} />
              </div>
              <div className="layout-group-item">
                <ListItem items={listItem} />
              </div>
            </Sidebar>
          </div>
          <div className="layout-column-large spacing-normal">
            {children}
          </div>
        </div>
      </div>

      {/* Modal add new user */}
      <Modal
        isOpen={stateUserModal}
        title="Enter user name"
        onCloseModal={handleCloseUserModal}
        btnNamePrimary="Save"
        onClickBtnPrimary={handleAddNewUser}>
        <Input
          size={SIZE.MEDIUM}
          value={inputValue}
          onChange={onChangeInput} />
      </Modal>

      {/* Modal Role */}
      <Modal
        isOpen={stateRoleModal}
        title="Enter role name"
        onCloseModal={handleCloseRoleModal}
        btnNamePrimary="Save"
        onClickBtnPrimary={handleAddNewRole}>
        <Input
          size={SIZE.MEDIUM}
          value={inputValue}
          onChange={onChangeInput} />
      </Modal>
    </>
  )
}

export default LayoutDefault;
