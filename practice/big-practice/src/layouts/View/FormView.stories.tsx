// library
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { ComponentMeta, ComponentStory } from '@storybook/react';

// Type
import { IListItem } from 'types/list';

// Components
import FormView from './index';

export default {
  title: 'FormView',
  component: FormView,
} as ComponentMeta<typeof FormView>;

const infoItem: IListItem['listItem'] = [
  { id: '1', value: 'Item 1' },
  { id: '2', value: 'Item 2' },
  { id: '3', value: 'Item 2' },
];

const list: IListItem['listItem'] = [
  { id: '1', value: 'Rule 1' },
  { id: '2', value: 'Rule 2' },
  { id: '3', value: 'Rule 3' },
  { id: '4', value: 'Rule 4' },
  { id: '5', value: 'Rule 5' },
  { id: '6', value: 'Rule 6' },
  { id: '7', value: 'Rule 7' },
  { id: '8', value: 'Rule 8' },
];

const Template: ComponentStory<typeof FormView> = (args) => <FormView {...args} />;

const Default = Template.bind({});
Default.args = {
  name: 'Name',
  hasAvatar: true,
  email: {
    title: 'List',
    icon: faEnvelope,
    listItem: infoItem,
  },

  rules: {
    title: 'List',
    icon: faEnvelope,
    listItem: list,
    count: list.length,

  },
};

export { Default };
