// library
import { memo } from 'react'
import clsx from 'clsx';

// components
import List from '@/components/List';
import Card from '@/components/Card';
import TextField from '@/components/TextField';

// Font Awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

//types
import { IListItem } from '@/types/list';
import { faClock, faEnvelope } from '@fortawesome/free-solid-svg-icons';

// css
import './view.css';

interface IProps {
  color?: string;
  name: string;
  avatar?: string;
  hasAvatar?: boolean;
  email?: IListItem;
  lastVisited?: IListItem;
  description?: string;
  users?: IListItem;
  roles?: IListItem;
  rules?: IListItem;
}

const FormView = ({
  color,
  name,
  avatar,
  hasAvatar,
  email,
  lastVisited,
  description,
  users,
  roles,
  rules,
}: IProps) => {

  return (
    <div className="view">
      <Card
        avatar={avatar!}
        color={color}
        description={description!}
        hasAvatar={hasAvatar}
        name={name} />

      <div className={clsx("view-info scroll", { "view-info--not-avatar": !hasAvatar })}>
        {email && (
          <TextField
            label={email.title}
            icon={<FontAwesomeIcon icon={faEnvelope} />}>
            {email.listItem.map(item => item.value)}
          </TextField>
        )}
        {lastVisited && (
          <TextField
            label={lastVisited.title}
            icon={<FontAwesomeIcon icon={faClock} />}>
            {lastVisited.listItem.map(item => item.value)}
          </TextField>
        )}

        {roles && <List {...roles} />}
        {rules && <List {...rules} />}
        {users && <List {...users} />}
      </div>
    </div>
  )
};

export default memo(FormView)
