//Font Awesome
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';

interface IListItem {
  icon?: IconDefinition,
  title: string;
  count?: number;
  listItem: { id: string; value: string }[];
}

export type { IListItem }
