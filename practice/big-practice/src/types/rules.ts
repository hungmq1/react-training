import { IRole } from './roles';
import { IUser } from './users';

interface IRule {
  id: number;
  name: string;
  description: string;
  roles?: IRole[];
  users?: IUser[];
}

export type { IRule };
