  interface IRole {
    id: number;
    name: string;
    color: string;
    rules?: number[];
    users?: number[];
  }

  export type { IRole };
