interface IUser {
  id: number;
  name: string;
  avatar: string;
  email: string;
  status: boolean;
  registered: Date;
  lastUpdate: Date;
  roles?: number[];
  rules?: number[];
  rulesAssigned?: number[];
  rulesRole?: { [key: string]: number[] };
}

export type { IUser }
