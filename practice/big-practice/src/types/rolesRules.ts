import { IRole } from './roles';

interface IRulesRoles {
  id: number;
  name: string;
  description: string;
  assignedDirectly: boolean;
  roles: (IRole | undefined)[];
  rulesRole?: { [key: string]: number[] };
}

export type {IRulesRoles};
