interface IItem {
  id?: string;
  value: string;
  highlight?: boolean;
  link?: string;
}

export type { IItem }
