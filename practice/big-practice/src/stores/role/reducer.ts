
import { ACTION } from '@/constants/actionTypes';
import { IRole } from '@/types/roles';

export interface IRoleState {
  roles: IRole[];
}

interface IRoleAction {
  type: string;
  payload: IRole;
}

export const initialRole: IRoleState = {
  roles: [],
};

const roleReducer = (stateRole = initialRole, action: IRoleAction): IRoleState => {
  const payload = action.payload;

  switch (action.type) {
    case ACTION.ADD:
      return {
        ...stateRole,
        roles: [...stateRole.roles, payload]
      };

    case ACTION.UPDATE:
      return {
        ...stateRole,
        roles: stateRole.roles.map((role) => {
          if (role.id === payload.id) {
            return {
              ...role,
              ...payload,
            };
          }
          return role;
        }),
      };


    case ACTION.DELETE:
      return {
        ...stateRole,
        roles: stateRole.roles.filter((role) => role.id !== payload.id),
      };
    default:
      return stateRole;
  }
};

export default roleReducer;
