// constants
import { ACTION } from '@/constants/actionTypes';

// types
import { IRole } from '@/types/roles';

const addRole = (payload: IRole) => {
  return {
    type: ACTION.ADD,
    payload,
  };
};

const updateRole = (payload: IRole) => {
  return {
    type: ACTION.UPDATE,
    payload,
  }
};

const deleteRole = (payload: IRole) => {
  return {
    type: ACTION.DELETE,
    payload,
  }
};

export {
  addRole,
  updateRole,
  deleteRole
};
