// constants
import { ACTION } from '@/constants/actionTypes';

// types
import { IUser } from '@/types/users';

interface IUserState {
  users: IUser[];
}

interface IUserAction {
  type: string;
  payload: IUser;
}

const initialUser: IUserState = {
  users: []
};

const userReducer = (stateUser = initialUser, action: IUserAction): IUserState => {
  switch (action.type) {
    case ACTION.ADD:
      return {
        ...stateUser,
        users: [...stateUser.users, action.payload],
      };

    case ACTION.UPDATE:
      const payload = action.payload;
      return {
        ...stateUser,
        users: stateUser.users.map((user) => {
          if (user.id === payload.id) {
            return {
              ...user,
              ...payload,
            };
          }
          return user;
        }),
      };

    case ACTION.DELETE:
      const payloadDelete = action.payload;
      return {
        ...stateUser,
        users: stateUser.users.filter((user) => user.id !== payloadDelete.id),
      };
    default:
      return stateUser;
  }
};

export type { IUserState, IUserAction };
export { initialUser };
export default userReducer;
