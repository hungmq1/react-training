// constants
import { ACTION } from '@/constants/actionTypes';

// types
import { IUser } from '@/types/users';

const addUser = (payload: IUser) => {
  return {
    type: ACTION.ADD,
    payload,
  };
};

const updateUser = (payload: IUser) => {
  return {
    type: ACTION.UPDATE,
    payload,
  }
};

const deleteUser = (payload: IUser) => {
  return {
    type: ACTION.DELETE,
    payload,
  }
};

export {
  addUser,
  updateUser,
  deleteUser
};
