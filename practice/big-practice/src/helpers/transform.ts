// helpers
import { findById } from "./find";

//types
import { IRule } from "@/types/rules";
import { IRulesRoles } from "@/types/rolesRules";
import { IRole } from "@/types/roles";

const resultRulesRoles = (
  rulesRole: { [key: string]: number[] },
  rules: IRule[],
  rulesActive: IRule[],
  roles: IRole[]
): IRulesRoles[] => {

  if (!rulesRole) {
    return [];
  }

  // Convert an Object to an Array
  const convertRulesRoles = Object.entries(rulesRole);

  return convertRulesRoles.map(([key, value]) => {
    const rule = findById<IRule>(rules, Number(key));
    let ruleSearch = {} as IRulesRoles;

    if (rule) {
      ruleSearch = {
        ...rule,
        assignedDirectly: rulesActive.includes(rule),
        roles: value.map((itemId) => findById<IRole>(roles!, itemId))
      };
    }

    return ruleSearch;
  });
};

export { resultRulesRoles };
