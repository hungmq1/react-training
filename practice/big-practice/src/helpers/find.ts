// types
import { IRole } from '@/types/roles';
import { IRule } from '@/types/rules';
import { IUser } from '@/types/users';

const findById = <T extends { id: number }>(array: T[], id: number): T | undefined => {

  return array.find((item) => item.id === id);
}

// Find list id
const findListId = <T extends 'user' | 'rule' | 'role', U extends 'UserRole' | 'UserRule' | 'RoleRule'>(
  typeId: T,
  typeData: U,
  id: number,
  listCheck: (IUser | IRule | IRole)[],
): number[] => {

  const list: number[] = [];
  let indexCheck: keyof (IRole | IUser) = 'id';
  let indexData: keyof (IRole | IUser) = 'rules';

  if (
    (typeId === 'role' && typeData === 'RoleRule') ||
    (typeId === 'user' && (typeData === 'UserRole' || typeData === 'UserRule'))
  ) {
    indexCheck = 'rules';
    indexData = 'id';
  }

  listCheck.forEach((item) => {
    const check = (item as any)[typeData]?.find((itemCheck: { id: number }) => itemCheck.id === id);

    if ((item as any)[indexCheck] === id && check !== undefined) {
      list.push((item as any)[indexData].id);
    }
  });

  return list;
};
export { findById, findListId }
