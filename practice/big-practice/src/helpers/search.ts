import { ISearch } from '@/types/search';

const filterData = <T extends ISearch>(data: T[], searchValue: string): T[] => {
  if (!data) {
    return [];
  }

  const searchLowerCase = searchValue.toLowerCase();
  return data.filter((item) => (searchLowerCase === ''
    ? item
    : item.name.toLowerCase().includes(searchLowerCase)))
}

export { filterData };
