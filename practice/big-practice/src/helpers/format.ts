import { IItem } from "@/types/item";

const formatItem = <T, K extends keyof T, U extends keyof T>(
  data: T[] | undefined,
  KeyId: K,
  KeyValue: U,
): IItem[] => {

  if (!data || data.length === 0) {
    return [];
  }

  const list = data.map((item) => ({
    id: item[KeyId] as any,
    value: item[KeyValue] as any,
  })) as IItem[];

  return list;
};

export { formatItem }
