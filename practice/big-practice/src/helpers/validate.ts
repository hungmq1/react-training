import { REGEXP_EMAIL } from '@/constants/regexp';

const validateEmail = (email: string): boolean => {
  const emailRegex = REGEXP_EMAIL;

  return emailRegex.test(email);
};

const validateEmpty = (value: string, errorMessage: string): string => {
  if (!value || value.trim().length === 0) {
    return errorMessage;
  }

  return '';
}


export { validateEmail, validateEmpty };
