import { createContext } from 'react';

// reducers
import { IRoleState, initialRole } from '@/stores/role/reducer';

// types
import { IRole } from '@/types/roles';

interface IRoleContext {
  stateRole: IRoleState;
  handleAddRole: (role: IRole) => void;
  handleUpdateRole: (role: IRole) => void;
  handleDeleteRole: (role: IRole) => void;
}

const RoleContext = createContext<IRoleContext>({
  stateRole: initialRole,
  handleAddRole: () => { },
  handleUpdateRole: () => { },
  handleDeleteRole: () => { },
});

export default RoleContext;
