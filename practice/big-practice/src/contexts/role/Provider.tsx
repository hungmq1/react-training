import { ReactNode, useReducer } from 'react';

// context
import RoleContext from './index';

// stores
import roleReducer, { initialRole } from '@/stores/role/reducer';
import { addRole, deleteRole, updateRole } from '@/stores/role/action';

// types
import { IRole } from '@/types/roles';


interface IRoleProvider {
  children: ReactNode;
}

const RoleProvider = ({ children }: IRoleProvider) => {
  const [stateRole, dispatchRole] = useReducer(roleReducer, initialRole);

  const handleAddRole = (role: IRole) => {
    dispatchRole(addRole(role));
  };

  const handleUpdateRole = (role: IRole) => {
    dispatchRole(updateRole(role));
  };

  const handleDeleteRole = (role: IRole) => {
    dispatchRole(deleteRole(role));
  };

  const contextValue = {
    stateRole,
    handleAddRole,
    handleUpdateRole,
    handleDeleteRole,
  };

  return (
    <RoleContext.Provider value={contextValue}>
      {children}
    </RoleContext.Provider>
  );
};

export default RoleProvider;
