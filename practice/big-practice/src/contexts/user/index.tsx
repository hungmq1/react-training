import { createContext } from 'react';

// Reducer
import { initialUser, IUserState } from '@/stores/user/reducer';
import { IUser } from '@/types/users';

interface IUserContext {
  stateUser: IUserState;
  handleAddUser: (user: IUser) => void;
  handleUpdateUser: (user: IUser) => void;
  handleDeleteUser: (user: IUser) => void;
}

const UserContext = createContext<IUserContext>({
  stateUser: initialUser,
  handleAddUser: () => {},
  handleUpdateUser: () => {},
  handleDeleteUser: () => {},
});

export default UserContext;
