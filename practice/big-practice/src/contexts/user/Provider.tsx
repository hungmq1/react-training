import { ReactNode, useReducer } from 'react';

// context
import UserContext from './index';

// stores
import userReducer, { initialUser } from '@/stores/user/reducer';
import { addUser, deleteUser, updateUser } from '@/stores/user/action';

// types
import { IUser } from '@/types/users';
interface IUserProvider {
  children: ReactNode;
}

const UserProvider = ({ children }: IUserProvider) => {
  const [stateUser, dispatchUser] = useReducer(userReducer, initialUser);

  const handleAddUser = (user: IUser) => {
    dispatchUser(addUser(user));
  };

  const handleUpdateUser = (user: IUser) => {
    dispatchUser(updateUser(user));
  };

  const handleDeleteUser = (user: IUser) => {
    dispatchUser(deleteUser(user));
  };

  const contextValue = {
    stateUser,
    handleAddUser,
    handleUpdateUser,
    handleDeleteUser,
  };

  return (
    <UserContext.Provider value={contextValue}>
      {children}
    </UserContext.Provider>
  );
};

export default UserProvider;
