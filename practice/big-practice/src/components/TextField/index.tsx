// library
import { memo, ReactNode } from 'react';

// css
import './textField.css'

interface IProps {
  label: string;
  icon?: ReactNode;
  children?: ReactNode;
}

const TextField = ({
  label,
  icon,
  children
}: IProps) => {

  return (
    <div>
      <div className="info">
        <div className="info-wrapper d-flex">
          <div className="info-icon">
            {icon}
          </div>
          <p>{label}:</p>
        </div>
        <div className="info-children">
          <p>{children}</p>
        </div>

      </div>
    </div>
  )
};

export default memo(TextField);

