import { faBrain } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ComponentMeta, ComponentStory } from '@storybook/react';

import TextField from './index';

export default {
  title: 'Text Field',
  component: TextField,
} as ComponentMeta<typeof TextField>;

const Template: ComponentStory<typeof TextField> = (args) => <TextField {...args} />;

const Default = Template.bind({});
Default.args = {
  label: 'Name Label',
  icon: <FontAwesomeIcon icon={faBrain} />,
  children: 'This is the description'
};

export { Default };
