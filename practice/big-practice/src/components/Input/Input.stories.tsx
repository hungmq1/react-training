// constants
import { SIZE } from '@/constants/enum';

// react
import { ComponentMeta, ComponentStory } from '@storybook/react';

// components
import Input from './index';

export default {
  title: 'Input',
  component: Input,
} as ComponentMeta<typeof Input>;

const Template: ComponentStory<typeof Input> = (args) => <Input {...args} />;

const Default = Template.bind({});
Default.args = {
  label: 'form input',
  name: 'Name',
  htmlFor: 'name',
  value: '',
  size: SIZE.SMALL
};

export { Default };
