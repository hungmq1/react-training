// css
import './input.css'

// constant
import { SIZE } from '@/constants/enum';

// react
import clsx from 'clsx';
import { LegacyRef, forwardRef, memo, useEffect, useRef } from 'react';

interface InputProps {
  label?: string;
  name?: string;
  value?: string;
  type?: string;
  htmlFor?: string;
  placeholder?: string;
  size?: SIZE.SMALL | SIZE.MEDIUM | SIZE.LARGE;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const Input = ({
  label,
  name,
  value,
  type,
  size,
  htmlFor,
  placeholder,
  onChange
}: InputProps, ref: LegacyRef<HTMLInputElement>) => {

  const inputRef = useRef<HTMLInputElement>(null);

  // auto focus
  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  }, []);

  return (
    <div className="input-box d-flex">
      {label &&
        (<label htmlFor={htmlFor} className="input-label">
          {label}:
        </label>)}
      <input
       ref={ref || inputRef}
        id={htmlFor}
        name={name}
        value={value}
        onChange={onChange}
        type={type}
        placeholder={placeholder}
        className={clsx("input-form", `input-${size}`)} />
    </div>
  );
};

export default memo(forwardRef(Input));

