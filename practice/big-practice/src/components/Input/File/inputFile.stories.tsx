// react
import { ComponentMeta, ComponentStory } from '@storybook/react';

// components
import InputFile from './index';

export default {
  title: 'Input/File',
  component: InputFile
} as ComponentMeta<typeof InputFile>;

const Template: ComponentStory<typeof InputFile> = (args) => <InputFile {...args} />;

const Default = Template.bind({});
Default.args = {
  label:'Avatar',
  labelText:'Upload New Photo',
  name: 'file',
  htmlFor:'avatar'
};

export { Default };
