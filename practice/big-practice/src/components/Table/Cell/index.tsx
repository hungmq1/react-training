// library
import { SIZE } from '@/constants/enum';
import clsx from 'clsx';
import { memo, ReactNode } from 'react'

// css
import './tableCell.css'

interface IProps {
  children?: ReactNode;
  type?: ReactNode;
  size?: SIZE.SMALL | SIZE.MEDIUM | SIZE.LARGE;
}

const TableCell = ({
  children,
  type,
  size
}: IProps) => {
  const TagName = type === "th" ? "th" : "td";

  return (
    <TagName className={clsx("table-cell", size && `table-cell-size-${size}`)}>
      {children}
    </TagName>
  )
};

export default memo(TableCell);
