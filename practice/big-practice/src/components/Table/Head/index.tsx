// library
import { memo, ReactNode } from 'react';

// css
import './tableHead.css'

interface IProps {
  children: ReactNode;
}

const TableHead = ({ children }: IProps) => {
  return (
    <thead className="table-head">
      {children}
    </thead>
  );
};

export default memo(TableHead);
