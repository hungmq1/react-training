import { memo, ReactNode } from 'react';
import clsx from 'clsx';

// css
import './tableRow.css'

interface IProps {
  children: ReactNode;
  isActive?: boolean;
  onRowClick?: (event: React.MouseEvent<HTMLTableRowElement>) => void;
}

const TableRow = ({
  children,
  isActive = false,
  onRowClick
}: IProps) => {

  const handleClick = (event: React.MouseEvent<HTMLTableRowElement>) => {
    if (onRowClick) {
      onRowClick(event);
    }
  }

  return (
    <tr
      onClick={handleClick}
      className={clsx('table-row', { 'table-row--active': isActive })}>
      {children}
    </tr>
  );
};

export default memo(TableRow);
