// library
import { memo, ReactNode } from 'react';

// css
import './tableBody.css'

interface IProps {
  children: ReactNode;
}

const TableBody = ({ children }: IProps) => {

  return (
    <tbody className="table-body scroll">
      {children}
    </tbody>
  );
};

export default memo(TableBody);
