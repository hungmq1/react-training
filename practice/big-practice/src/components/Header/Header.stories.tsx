// react
import { ComponentStory, ComponentMeta } from '@storybook/react';

// component
import Header from './index';

export default {
  title: 'Header',
  component: Header,
} as ComponentMeta<typeof Header>;

const Template: ComponentStory<typeof Header> = (args) => <Header {...args} />;

const Default = Template.bind({});
Default.args = {
  children: 'Header',
};

export { Default };
