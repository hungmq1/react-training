import { ReactNode } from 'react';
import './header.css'

interface IProps {
  children: ReactNode
}

const Header = ({ children }: IProps) => {
  return (
    <header className="header-wrapper">
      <h1 className="header-title">
        {children}
      </h1>
    </header>
  )
}

export default Header;
