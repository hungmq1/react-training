import clsx from 'clsx';

// Font Awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserShield } from '@fortawesome/free-solid-svg-icons';

// css
import './label.css';

interface IProps {
  icon?: boolean;
  name: string;
  color?: string;
  disable: boolean;
}

const Label = ({
  icon,
  name,
  color,
  disable
}: IProps) => {

  return (
    <div className="label d-flex d-flex-center">
      {icon && (
        <FontAwesomeIcon icon={faUserShield} className="label-icon" color={color} />
      )}
      <p className={clsx("label-name", { "label-name--disable": disable })}>
        {name}
      </p>
    </div>
  );
};

export default Label;
