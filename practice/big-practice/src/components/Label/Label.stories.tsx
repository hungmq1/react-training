import { ComponentMeta, ComponentStory } from '@storybook/react';

// Components
import Label from './index';

export default {
  title: 'Label',
  component: Label,
} as ComponentMeta<typeof Label>;

const Template: ComponentStory<typeof Label> = (args) => <Label {...args}/>;

const Default = Template.bind({});
Default.args = {
  icon: true,
  color: '#1ca1c1',
  name: 'Label',
};

const Disable = Template.bind({});
Disable.args = {
  name: 'Label',
  disable: true,
};

export { Default, Disable };
