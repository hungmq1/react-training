import { ComponentMeta, ComponentStory } from '@storybook/react';

// component
import Popover from './index';

export default {
  title: 'Popover',
  component: Popover
} as ComponentMeta<typeof Popover>

const listPopoverItem = [
  {
    label: 'Action One',
    onClick: () => alert('You clicked Action 1'),
  },
  {
    label: 'Action Two',
    onClick: () => alert('You clicked Action 2'),
  },
];

const Template: ComponentStory<typeof Popover> = (args) => <Popover {...args} />;

const Default = Template.bind({});
Default.args = {
  labelText: 'New',
  items: listPopoverItem,
};

export { Default }
