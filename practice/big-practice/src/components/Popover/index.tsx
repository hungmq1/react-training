// css
import './popover.css';

// library
import { useState } from 'react';

// constants
import { SIZE, TYPE } from '@/constants/enum';

// FontAwesome
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// components
import Button from '@/components/Button';

interface IPopoverItem {
  label: string;
  onClick: () => void;
}

interface IPopover {
  labelText: string;
  items: IPopoverItem[];
}

const Popover = ({ labelText, items }: IPopover) => {
  const [isPopoverOpen, setIsPopoverOpen] = useState(false);

  const handleClick = () => {
    setIsPopoverOpen(!isPopoverOpen);
  };

  const handleItemClick = (onClick: () => void) => {
    setIsPopoverOpen(false);
    onClick();
  };

  return (
    <div className="popover-wrapper">
      <Button
        label={labelText}
        onClick={handleClick}
        size={SIZE.LARGE}
        icon={<FontAwesomeIcon icon={faPlus} />}
        typeButton={TYPE.PRIMARY}
      />

      {isPopoverOpen && (
        <div className="popover">
          {items.map((item, index) => (
            <a
              key={index}
              className="popover-item d-flex d-flex-center"
              onClick={() => handleItemClick(item.onClick)}>
              {item.label}
            </a>
          ))}
        </div>
      )}
    </div>
  );
};

export default Popover;
