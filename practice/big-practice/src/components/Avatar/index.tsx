// library
import clsx from 'clsx';
import { memo } from 'react';

// constant
import { SIZE } from '@/constants/enum';

// CSS
import './avatar.css';

interface IProps {
  avatar?: string;
  name: string;
  circle?: boolean;
  size?: SIZE.SMALL | SIZE.MEDIUM | SIZE.LARGE;
  color?: string;
}

const Avatar = ({
  avatar,
  name,
  circle,
  size,
  color
}: IProps) => {

  const nameAvatar = name ? name.charAt(0).toUpperCase() : "";

  const renderImg = () => {
    return (
      <img
        src={avatar}
        alt={name}
        className={clsx("avatar", `avatar-${size}`, { "avatar-circle": circle })}
      />
    )
  };

  const renderCircle = () => {
    return (
      <div className={clsx("avatar d-flex d-flex-center", `avatar-${size}`, { "avatar-circle": circle })}
        style={{ backgroundColor: color }}>
        {nameAvatar}
      </div>
    )
  };

  return (
    <>
      {avatar ? renderImg() : renderCircle()}
    </>
  )
};

export default memo(Avatar);
