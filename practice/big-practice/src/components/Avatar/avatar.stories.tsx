// constants
import { SIZE } from '@/constants/enum';

// react
import { ComponentMeta, ComponentStory } from '@storybook/react';

// component
import Avatar from './index';

export default {
  title: 'Avatar',
  component: Avatar
} as ComponentMeta<typeof Avatar>;

const Template: ComponentStory<typeof Avatar> = (args) => <Avatar {...args} />;

const AvatarCircle = Template.bind({});
AvatarCircle.args = {
  name: 'name',
  size: SIZE.SMALL,
  circle: true,
};

const Small = Template.bind({});
Small.args = {
  name: 'name',
  size: SIZE.SMALL,
};

const Medium = Template.bind({});
Medium.args = {
  name: 'name',
  size: SIZE.MEDIUM,
};

const Large = Template.bind({});
Large.args = {
  name: 'name',
  size: SIZE.LARGE,
};

const Image = Template.bind({});
Image.args = {
  avatar: 'https://s.memehay.com/files/posts/20201020/a-the-a-phien-ban-nhan-vat-anime-fc02f8bbb84d5a231016d6262a617a90.jpg',
  size: SIZE.MEDIUM,
  circle: true,
};

export { AvatarCircle, Small, Medium, Large, Image }
