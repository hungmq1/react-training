// library
import { faAddressBook } from '@fortawesome/free-solid-svg-icons';
import { ComponentMeta, ComponentStory } from '@storybook/react';

// Components
import List from './index';

export default {
  title: 'List',
  component: List,
} as ComponentMeta<typeof List>;

const listItem = [
  { id: '1', value: 'Item 1' },
  { id: '2', value: 'Item 2' },
  { id: '3', value: 'Item 3' },
];


const Template: ComponentStory<typeof List> = (args) => <List {...args} />;

const Default = Template.bind({});
Default.args = {
  icon: faAddressBook,
  title: 'List Title',
  count: listItem.length,
  listItem: listItem,
};

export { Default };
