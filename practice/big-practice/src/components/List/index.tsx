// library
import { memo } from 'react';

//Font Awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// components
import Item from './Item';

//css
import './list.css';

// types
import { IListItem } from '@/types/list';

const List = ({
  icon,
  title,
  count,
  listItem
}: IListItem) => {
  const countItem = count ? count : count === 0 ? "0" : "";

  return (
    <ul className="list">
      <div className="d-flex list-header">
        <div>
          <FontAwesomeIcon icon={icon!} className="list-icon" />
        </div>
        <p className="list-header-label">
          {title} {`(${countItem})`}
        </p>
      </div>

      {listItem.map((item) => (
        <Item
          key={item.id}
          children={item.value}
        />
      ))}
    </ul>
  );

};

export default memo(List);
