// library
import { ComponentMeta, ComponentStory } from '@storybook/react';

// Components
import Item from './index';

export default {
  title: 'List/Item',
  component: Item,
} as ComponentMeta<typeof Item>;

const Template: ComponentStory<typeof Item> = (args) => <Item {...args} />;

const Default = Template.bind({});
Default.args = {
  children: 'This is Item',
};

export { Default };
