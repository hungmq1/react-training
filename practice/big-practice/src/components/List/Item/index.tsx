// library
import { memo } from 'react';

// css
import './item.css'

interface Props {
  children: string;
  id?: string;
}

const Item = ({ children, id }: Props) => {
  return (
    <li className="item-link" id={id}>
    <a>
      {children}
    </a>
    </li>
  )
};

export default memo(Item);

