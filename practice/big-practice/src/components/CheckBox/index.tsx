import React, { useCallback } from 'react';

// css
import '@/components/CheckBox/index.css';

interface IProps {
  id: string;
  children: React.ReactNode | string;
  checked: boolean;
  onChange: (id: string, checked: boolean) => void;
}

const CheckBox = ({ id, children, checked, onChange }: IProps) => {
  const handleClick = useCallback(
    () => {
      onChange(id, !checked);
    },
    [id, checked, onChange]
  );

  return (
    <div id={id} className="checkbox d-flex" >
      <input
        type="checkbox"
        checked={checked}
        className="checkbox-input"
        onChange={handleClick}
      />

      <label className="checkbox-label" onClick={handleClick}>
        {children}
      </label>
    </div>
  );
};

export default CheckBox;
