import { ComponentMeta, ComponentStory } from '@storybook/react';

// Components
import Checkbox from './index';

export default {
  title: 'Checkbox',
  component: Checkbox,
} as ComponentMeta<typeof Checkbox>;

const Template: ComponentStory<typeof Checkbox> = (args) => <Checkbox {...args} />;

const Default = Template.bind({});
Default.args = {
  checked: false,
  children: 'No Check'
};

const Checked = Template.bind({});
Checked.args = {
  checked: true,
  children: 'Checked'
};

export { Default, Checked };
