// css
import './status.css';

// library
import clsx from 'clsx';

// constant
import { STATUS } from '@/constants/enum';

interface IProps {
  isActive: boolean;
}

const Status = ({ isActive }: IProps) => {

  return (
    <div className={clsx("status", { "status-active": isActive })}>
      { isActive ? STATUS.ACTIVE : STATUS.NOT_ACTIVE }
    </div>
  );
};

export default Status;
