import { Suspense, lazy, memo, useCallback, useState } from 'react';

// css
import '@/components/ColorPicker/colorPicker.css';

type ColorPickerProps = {
  defaultColor: string;
  onColorChange: (color: string) => void;
};

const ColorPicker = ({
  defaultColor,
  onColorChange,
}: ColorPickerProps) => {
  const [currentColor, setCurrentColor] = useState(defaultColor);
  const [showPicker, setShowPicker] = useState(false);

  const handleColorChange = useCallback((color: any) => {
    setCurrentColor(color.hex);
    onColorChange(color.hex);
  }, [onColorChange]);

  const handleShowPicker = useCallback(() => {
    setShowPicker(!showPicker);
  }, [showPicker]);

  const GithubPickerLazy = lazy(() => import('react-color').then((module) => ({ default: module.GithubPicker })));

  return (
    <div className="color-picker d-flex">
      <label className="color-picker-label"> Color: </label>
      <button
        style={{ backgroundColor: currentColor }}
        className="color-picker-area"
        onClick={handleShowPicker}
      >
        {showPicker && (
          <Suspense fallback={<div>Loading...</div>}>
            <GithubPickerLazy
              color={currentColor}
              onChangeComplete={handleColorChange}
            />
          </Suspense>
        )}
      </button>
    </div>
  );
};

export default memo(ColorPicker);
