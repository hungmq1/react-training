// library
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { memo } from 'react';

// css
import './appbar.css';

// components
import IconButton from '@/components/Button/IconButton';
import Status from '@/components/Status';

interface IProps {
  title: string;
  isStatus?: boolean;
  isActive?: boolean;
  isIcon?: boolean;
  icon?: IconDefinition;
  itemClick?: () => void
}

const Appbar = ({
  title,
  isStatus,
  isActive,
  itemClick,
  isIcon,
  icon
}: IProps) => {

  return (
    <div className="appbar d-flex d-flex-space-between">
      <>
        <h4 className="appbar-title">
          {title}
        </h4>
        {isStatus && <Status isActive={isActive!} />}
        {isIcon && <IconButton icon={icon!} onClick={itemClick!} />}
      </>
    </div>
  );
};

export default memo(Appbar);
