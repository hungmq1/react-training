// library
import { ComponentMeta, ComponentStory } from '@storybook/react';

// FontAwesome
import { faPen } from '@fortawesome/free-solid-svg-icons';

// components
import Appbar from './index';

export default {
  title: 'Appbar',
  component: Appbar
} as ComponentMeta <typeof Appbar>;

const Template: ComponentStory<typeof Appbar> = (args) => <Appbar {...args} />;

const Default = Template.bind({});
Default.args = {
  title: 'Title',
};

const hasIcon = Template.bind({});
hasIcon.args = {
  title: 'Title',
  isIcon: true,
  icon: faPen
};

const hasStatus = Template.bind({});
hasStatus.args = {
  title: 'Title',
  isStatus: true,
  isIcon: true,
  icon: faPen
};

export {Default, hasIcon, hasStatus}
