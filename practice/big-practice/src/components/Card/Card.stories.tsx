// library
import { ComponentMeta, ComponentStory } from '@storybook/react';

// components
import Card from './index';

export default {
  title: 'Card',
  component: Card
} as ComponentMeta<typeof Card>

const Template: ComponentStory<typeof Card> = (args) => <Card {...args} />;

const HasImage = Template.bind({});
HasImage.args = {
  name: 'name',
  avatar: '',
  color: 'red',
  hasAvatar: true
};

const HasDescription = Template.bind({});
HasDescription.args = {
  name: 'name',
  description: 'This is description'
};

export { HasImage, HasDescription };
