// library
import { memo } from 'react';

// components
import Avatar from '@/components/Avatar';

// constants
import { SIZE } from '@/constants/enum';

// css
import './card.css'

interface IProps {
  hasAvatar?: boolean;
  avatar: string
  name: string;
  description: string;
  color?: string
}

const Card = ({
  hasAvatar,
  avatar,
  name,
  description,
  color
}: IProps) => {

  return (
    <>
      <div className="card-info">
        <div className="d-flex d-flex-center">
          {hasAvatar && (
            <Avatar
              name={name}
              avatar={avatar}
              size={SIZE.LARGE}
              color={color} />
          )}
        </div>
        <h3 className="card-name">{name}</h3>
      </div>

      <p className="card-des spacing-normal">{description}</p>
    </>
  )
};

export default memo(Card);
