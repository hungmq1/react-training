// react
import { ComponentMeta, ComponentStory } from '@storybook/react';

// components
import RadioButton from './index';

export default {
  title: 'Radio Button',
  component: RadioButton,
} as ComponentMeta<typeof RadioButton>;

const Template: ComponentStory<typeof RadioButton> = (args) => <RadioButton {...args} />;

const Default = Template.bind({});
Default.args = {
  count: 5,
  id: 'htmlFor',
  isChecked: false,
  label: 'Radio Default',
  onChange: () => alert('checked!!')
};

const Checked = Template.bind({});
Checked.args = {
  count: 5,
  id: 'htmlFor',
  isChecked: true,
  label: 'Radio Checked',
  onChange: () => alert('checked!!')
};

export { Default, Checked };
