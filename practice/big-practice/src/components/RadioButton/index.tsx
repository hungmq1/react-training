import { memo } from 'react';

// css
import './radioButton.css';

interface IProps {
  label: string;
  count: number;
  isChecked?: boolean;
  id: string;
  onChange: () => void;
}

const RadioButton = ({
  label,
  count,
  isChecked,
  id,
  onChange
}: IProps) => {
  return (
    <div className="d-flex radio-button" onClick={onChange}>
      <input
        className="radio-button-input"
        type='radio'
        id={id}
        checked={isChecked}
        onChange={onChange} />

      <div className="d-flex radio-button-label">
        <label htmlFor={id}>{label}</label>
        <p>({count})</p>
      </div>
    </div>
  )
};

export default memo(RadioButton);
