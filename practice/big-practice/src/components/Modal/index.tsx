// FontAwesome
import { faXmark } from '@fortawesome/free-solid-svg-icons';

// css
import './modal.css'

// library
import { forwardRef, LegacyRef, memo, ReactNode } from 'react';

// constant
import { SIZE, TYPE } from '@/constants/enum';

// component
import Button from '@/components/Button';
import IconButton from '../Button/IconButton';

interface IModal {
  isOpen: boolean;
  title: string;
  btnNamePrimary: string;
  btnNameSecondary?: string;
  onClickBtnPrimary: () => void;
  onClickBtnSecondary?: () => void;
  onCloseModal: () => void;
  children: ReactNode;
}

const Modal = ({
  isOpen,
  title,
  btnNamePrimary,
  btnNameSecondary,
  onCloseModal,
  onClickBtnPrimary,
  onClickBtnSecondary,
  children
}: IModal, ref: LegacyRef<HTMLInputElement>) => {

  return (
    <>
      {isOpen && (
        <div className="modal-overlay d-flex d-flex-center">
          <div className="modal" ref={ref}>
            <div className="modal-header d-flex d-flex-space-between">
              <h3 className="modal-title">{title}</h3>
              <IconButton icon={faXmark} onClick={onCloseModal} />
            </div>
            <div className="modal-body d-flex">
              {children}

              {btnNamePrimary && (
                <Button
                  label={btnNamePrimary}
                  size={SIZE.SMALL}
                  typeButton={TYPE.PRIMARY}
                  onClick={onClickBtnPrimary}
                />
              )}

              {btnNameSecondary && onClickBtnSecondary && (
                <Button
                  label={btnNameSecondary}
                  size={SIZE.SMALL}
                  onClick={onClickBtnSecondary}
                />
              )}
            </div>
          </div>
        </div>
      )}
    </>
  )
}

export default memo(forwardRef(Modal));

