import { ComponentMeta, ComponentStory } from '@storybook/react';

import Modal from './index';

export default {
  title: 'Modal',
  component: Modal,
} as ComponentMeta<typeof Modal>;

const Template: ComponentStory<typeof Modal> = (args) => <Modal {...args} />;

const Default = Template.bind({});
Default.args = {
  isOpen: true,
  title: 'This is title'
};

const HasButton = Template.bind({});
HasButton.args = {
  isOpen: true,
  title: 'This is title',
  btnNamePrimary: 'Button 1'
};

export { Default, HasButton };
