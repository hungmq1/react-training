// constants
import { SIZE, TYPE } from '@/constants/enum';

// FontAwesome
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// react
import { ComponentMeta, ComponentStory } from '@storybook/react';

// component
import Button from './index'

export default {
  title: 'Button',
  component: Button
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

const LargeButton = Template.bind({});
LargeButton.args = {
  typeButton: TYPE.PRIMARY,
  label: 'New',
  size: SIZE.LARGE,
  icon: <FontAwesomeIcon icon={faPlus} />,
  onClick: () => {
    alert('Large Button');
  },
};

const SecondaryButton = Template.bind({});
SecondaryButton.args = {
  label: 'Secondary',
  size: SIZE.SMALL,
  onClick: () => {
    alert('Secondary Button');
  },
};

const PrimaryButton = Template.bind({});
PrimaryButton.args = {
  typeButton: TYPE.PRIMARY,
  label: 'Primary',
  size: SIZE.SMALL,
  onClick: () => {
    alert('Primary Button');
  },
};

export { LargeButton, SecondaryButton, PrimaryButton };
