// css
import './iconButton.css'

// FontAwesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';


interface IProps {
  icon: IconDefinition,
  onClick: () => void
}
const IconButton = ({
  icon,
  onClick
}: IProps) => {

  return (
    <div className="icon-block d-flex d-flex-center"
      onClick={onClick}>
      <i className="icon"
        aria-hidden="true">
        <FontAwesomeIcon icon={icon} />
      </i>
    </div >
  )
};

export default IconButton;
