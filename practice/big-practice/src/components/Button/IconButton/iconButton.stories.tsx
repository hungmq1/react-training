// components
import IconButton from './index';

// react
import { ComponentMeta, ComponentStory } from '@storybook/react';

// FontAwesome
import { faPen } from '@fortawesome/free-solid-svg-icons';

export default {
  title: 'IconButton',
  component: IconButton
} as ComponentMeta<typeof IconButton>;

const Template: ComponentStory<typeof IconButton> = (args) => <IconButton {...args} />;

const Default = Template.bind({});
Default.args = {
  icon: faPen ,
  onClick: () => {
    alert('Icon Button');
  },
}

export {Default}
