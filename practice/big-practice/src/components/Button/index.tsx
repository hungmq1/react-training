// constant
import { SIZE, TYPE } from '@/constants/enum';

// react
import clsx from 'clsx';
import { ReactNode } from 'react';

// css
import './button.css'

interface ButtonProps {
  typeButton?: TYPE.PRIMARY | TYPE.SECONDARY;
  label: string;
  size?: SIZE.SMALL | SIZE.MEDIUM | SIZE.LARGE;
  onClick: () => void;
  disabled?: boolean;
  icon?: ReactNode
}

const Button = ({
  typeButton,
  label,
  size,
  onClick,
  disabled,
  icon }: ButtonProps) => {

  return (
    <div>
      <button
        className={clsx("btn", typeButton && `btn-${typeButton}`, size && `btn-${size}`)}
        onClick={onClick}
        disabled={disabled}>
        {icon && (<i className="btn-icon">{icon}</i>)}
        {label}
      </button>
    </div>
  )
}

export default Button;
