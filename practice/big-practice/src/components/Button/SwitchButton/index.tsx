// react
import React from 'react';

// css
import './switchButton.css';

interface IProps {
  isChecked: boolean;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const SwitchButton = ({
  isChecked,
  onChange
}: IProps) => {

  return (
    <label className="switch">
      <input
        type="checkbox"
        checked={isChecked}
        onChange={onChange}
      />
      <span className="slider round"></span>
    </label>
  );
}

export default SwitchButton;
