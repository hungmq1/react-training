// css
import './searchBar.css'

// library
import { memo } from 'react';

// constant
import { SIZE } from '@/constants/enum';

// FontAwesome
import { faXmark } from '@fortawesome/free-solid-svg-icons';

// components
import Input from '@/components/Input';
import IconButton from '@/components/Button/IconButton';

interface IProps {
  inputValue: string | undefined;
  onClose?: () => void;
  onSearch?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const SearchBar = ({
  inputValue,
  onClose,
  onSearch
}: IProps) => {

  // Change input
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    onSearch!(event);
  };

  return (
    <div className="search d-flex">
      <Input
        onChange={handleInputChange!}
        size={SIZE.LARGE}
        value={inputValue}
        placeholder="Search" />

      <IconButton icon={faXmark} onClick={onClose!} />
    </div>
  )
};

export default memo(SearchBar);
