// library
import { memo } from 'react';
import { NavLink } from 'react-router-dom';

// FontAwesome
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

//css
import './item.css'

interface IProps {
  name: string;
  icon?: IconDefinition;
  path: string;
}

const SidebarItem = ({
  name,
  icon,
  path
}: IProps) => {
  return (
    <>
      <NavLink to={path} className="sidebar-item d-flex">
        <i className="sidebar-item-icon">
          <FontAwesomeIcon icon={icon!} />
        </i>
        <p>{name}</p>
      </NavLink>
    </>
  )
};

export default memo(SidebarItem);
