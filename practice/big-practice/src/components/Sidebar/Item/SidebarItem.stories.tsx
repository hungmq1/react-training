// library
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { withRouter } from 'storybook-addon-react-router-v6';

// components
import Item from './index';

export default {
  title: 'Sidebar-Item',
  component: Item,
  decorators: [withRouter],
} as ComponentMeta<typeof Item>;

const Template: ComponentStory<typeof Item> = (args) => <Item {...args} />;

const Default = Template.bind({});
Default.args = {
  name: 'Sidebar-Item',
  icon: faUser,
  path: '/',
};

export {Default}
