// components
import SidebarItem from '@/components/Sidebar/Item';

// FontAwesome
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';

interface IProps {
  items: {
    name: string;
    icon: IconDefinition;
    path: string;
  }[];
}

const ListItem = ({ items }: IProps) => {

  return (
    <ul className="sidebar-list">
      {items.map((item) => (
        <SidebarItem
          key={item.name}
          name={item.name}
          icon={item.icon}
          path={item.path} />
      ))}
    </ul>
  );
};

export default ListItem;
