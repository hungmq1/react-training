// library
import { ReactNode } from 'react';

// css
import './sidebar.css';

interface IProps {
  children?: ReactNode;
}

const Sidebar = ({ children }: IProps) => {

  return (
    <div className="sidebar">
      {children}
    </div>
  );
};

export default Sidebar;
