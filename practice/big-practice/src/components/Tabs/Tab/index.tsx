// library
import { memo } from 'react';
import clsx from 'clsx';

// types
import { ITab } from 'types/tab';

// css
import './tab.css';

const Tab = ({ value, index, title, onClick }: ITab) => (
  <button
    value={value}
    className={clsx("tab", { "tab--active": index === value })}
    onClick={onClick}
  >
    {title}
  </button>
);

export default memo(Tab);
