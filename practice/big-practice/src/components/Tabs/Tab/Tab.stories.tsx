// library
import { ComponentMeta, ComponentStory } from '@storybook/react';

// components
import Tab from './index';

export default {
  title: 'Tabs/Tab',
  component: Tab,
} as ComponentMeta<typeof Tab>;

const Template: ComponentStory<typeof Tab> = (args) => <Tab {...args} />;

const Default = Template.bind({});
Default.args = {
  value: '1',
  title: 'Item one',
};

const TabActive = Template.bind({});
TabActive.args = {
  value: '1',
  title: 'Item one',
  index: '1',
};

export { Default, TabActive };
