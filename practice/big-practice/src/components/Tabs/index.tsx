// library
import { memo } from 'react';

// types
import { ITab } from 'types/tab';

// components
import Tab from './Tab';

// css
import './tabs.css';

interface IProps {
  list: Omit<ITab, 'index' | 'onClick'>[];
  index: string;
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const Tabs = ({ list, onClick, index }: IProps) => (
  <div className="tabs d-flex d-flex-center">
    {list.map((item) => (
      <Tab
        key={item.value}
        value={item.value}
        title={item.title}
        index={index}
        onClick={onClick}
      />
    ))}
  </div>
);

export default memo(Tabs);
