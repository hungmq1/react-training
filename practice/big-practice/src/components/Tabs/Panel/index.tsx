// library
import { memo, ReactNode } from 'react';

// css
import './tabPanel.css';

interface IProps {
  index: string;
  value: string;
  children: ReactNode;
}

const TabPanel = ({
  index,
  value,
  children
}: IProps) => {

  if (index !== value) {
    return null;
  }

  return (
    <div className="tab-panel">
      {children}
    </div>
  )
};

export default memo(TabPanel);
