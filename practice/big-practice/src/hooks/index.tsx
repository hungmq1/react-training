import { useContext } from 'react';
import useSWR from 'swr';

// context
import UserContext from '@/contexts/user';
import RoleContext from '@/contexts/role';

// services
import { getRoles } from '@/services/role';
import { getUsers } from '@/services/user';
import { getRules } from '@/services/rule';

// useUserStore
const useUserStore = () => {
  return useContext(UserContext);
};

// useRoleStore
const useRolesStore = () => {
  return useContext(RoleContext);
};

// Hook fetch API Roles
const useRoles = () => {
  return useSWR('/roles', getRoles);
};

// Hook fetch API Users
const useUsers = () => {
  return useSWR('/users', getUsers);
};

// Hook fetch API Rules
const useRules = () => {
  return useSWR('/rules', getRules);
};


export { useUserStore, useRolesStore, useRoles, useUsers, useRules }
