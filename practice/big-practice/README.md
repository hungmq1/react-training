# React-training

# Big practice : User manager

## Diagram
![Relationship](../big-practice/src/assets/images/relationshipTable.drawio.png);
![Hook](../big-practice/src/assets/images/HookDiagram.drawio.png);

### Overview

- Build a website for user management
- Description: user data is stored to json-server, user can use CRUD and search name, change role and rule
- Design:
  -User manager: https://webix.com/demos/user-manager/

### Targets

- Apply what you have read about the main concepts of ReactJS
- And know how to use Storybook
- Use Hooks

### Requirements

- Create app user manager
- CRUD
- Search, filter user
- Role
- Rule
- Apply storybook

### Information

- Timeline
  - Estimate day: 15 days
  - Actual day:
- Techniques:
  - HTML/CSS
  - Typescript
  - Storybook
  - Vite
  - Gitlab
  - Json-server
  - Axios
  - ESLint Extension
  - Prettier Extension
  - ESLint with TypeScript

- Editor: Visual Studio Code.

### Development Environment
- Node v16.16
- pnpm v7.19.0
- ReactJS v18.2.0
- Storybook ReactJS v6.5.16
- Vite v4.2.0
- Eslint v8.36.0
- Prettier v2.7.1
- TypeScript v4.9.3
- axios v1.3.4

### Main App Feature

- User:
  - Create new user
  - View user info
  - Edit General
  - Edit Rules
  - Edit Roles
  - Update user
  - Delete user
  - search/filter user

- Role:
  - Create new role
  - View role info
  - Edit General
  - Edit Rules
  - Edit Members
  - Update role
  - Delete role
  - search/filter role

- Rule:
  - Create new rule
  - View rule info
  - search/filter rule
  -
### Install Json server (You can skip if you installed json-server)
> $ pnpm install -g json-server

### Run server

> pnpm run server ( yarn server )

### Run project

> pnpm run start ( yarn start )

follow at:

<a href="http://127.0.0.1:5173/">http://127.0.0.1:5173/</a>

### Run storybook

> pnpm run storybook

### Note: Since my project is using Node v.16 so if you want to run storybook please execute below command,
### It will help you to configure the right Node version

export NODE_OPTIONS=--openssl-legacy-provider

follow at:

<a href="http://localhost:6006/">http://localhost:6006/</a>
